from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'fast metrics using cython/numpy',
  ext_modules = cythonize("fast_metrics.pyx"),
)
