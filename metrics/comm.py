#!/usr/bin/env python

def dump_comms(inputfile):
	commdict = {}
	ch = set()
	for line in open(inputfile):

		value, key  = line.split()
		#print(line.split())
		ch.add(int(key))
		if key not in commdict:
			commdict[key] = []
		
		commdict[key].append(int(value)-1)


	#print(ch)
	for key in commdict:
		#pass
		print(commdict[key])

if __name__ == '__main__':

	comm_file = '/home/alamages/Projects/build-kcores-Desktop-Debug/ee'
	#comm_official_file = '/home/alamages/Projects/c-cores-java/100/community_1.dat'

	#print('My communities')
	#dump_comms(comm_file)

	#print('\nOfficial communities')
	dump_comms(comm_file)