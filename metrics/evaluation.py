#!/usr/bin/env python2
from __future__ import division
import sys
import math
import argparse
from fast_metrics import *

class ClusterDict(dict):
    def __getitem__(self, key):
        if key not in self:
            self[key] = {}
        return dict.__getitem__(self, key)

class Evaluation:
    def __init__(self):
        self.__setup()

    def __setup(self):
        self.__nodes_num = 0
        self.__max_cluster = {"alg" : 0, "real" : 0}
        self.__node_clusters = ClusterDict()

    def clear(self):
        self.__setup()

    #mode can be real|alg
    #real: real clusters
    #alg: the cluster found from a cluster algorithm
    def LoadFile(self, inputfile, mode):
        if not mode == "real" and not mode == "alg":
            print("potato")
            sys.exit(1)

        storekey = mode
        count_nodes = 0
        with open(inputfile, 'r') as filein:
            for line in filein.readlines():
                if line == '\n' or not line[0].isdigit():
                    continue

                #split and convert to int
                node_id, cluster_id = map(int, line.split())

                self.__node_clusters[node_id][storekey] = cluster_id
                count_nodes += 1

                if cluster_id > self.__max_cluster[mode]:
                    self.__max_cluster[mode] = cluster_id

        if not self.__nodes_num:
            self.__nodes_num = count_nodes
        elif not self.__nodes_num == count_nodes:
            print('ERRAR not same number of nodes in given files, mode: {}'.format(mode))
            sys.exit(1)

    def __GetTwoDimArray(self, dim1, dim2):
        mylist = []
        for j in range(dim1):
            mylist.append([0]*dim2)

        return mylist

    def NMI(self):
        #initialize the arrays
        probc = [0] * (self.__max_cluster["alg"]+1)
        probcl = [0] * (self.__max_cluster["real"]+1)

        #probinter = array[max_cluster["alg"]+1][max_cluster["real"]+1]
        probinter = self.__GetTwoDimArray(len(probc), len(probcl))

        for node_id in self.__node_clusters:
            alg_cluster = self.__node_clusters[node_id]["alg"]
            real_cluster = self.__node_clusters[node_id]["real"]

            probc[alg_cluster] += 1
            probcl[real_cluster] += 1
            probinter[alg_cluster][real_cluster] += 1

        #NOTE in python3 by default division of int returns floats
        #in python2 you need to either cast with float() or import division from future

        #same as self.node_num but is more readable:
        nodes_size = self.__nodes_num#len(self.__node_clusters)

        for i in range(len(probc)):
            probc[i] = probc[i] / nodes_size

        for i in range(len(probcl)):
            probcl[i] = probcl[i] / nodes_size

        for i in range(len(probinter)):
            for j in range(len(probinter[i])):
                probinter[i][j] = probinter[i][j] / nodes_size

        in_value = 0
        for i in range(len(probc)):
            for j in range(len(probcl)):
                if probinter[i][j] != 0 and probc[i] !=0 and probcl[j] != 0:
                    in_value += probinter[i][j] * math.log10(probinter[i][j] / (probc[i]*probcl[j]))

        hc_value = 0
        for i in range(len(probc)):
            if probc[i] != 0:
                hc_value += probc[i] * math.log10(probc[i])

        hc_value = -hc_value

        hcl_value = 0
        for i in range(len(probcl)):
            if probcl[i] != 0:
                hcl_value += probcl[i] * math.log10(probcl[i])
        hcl_value = -hcl_value

        #nmi = 2 * (in_value / (hc+hcl))
        return 2 * (in_value / (hc_value+hcl_value))

    def RI(self):
        tfpn = [[0, 0], [0, 0]]
        for i in range(self.__nodes_num):
            for j in range(i+1, self.__nodes_num):
                if self.__node_clusters[i+1]["alg"] == self.__node_clusters[j+1]["alg"]:
                    if self.__node_clusters[i+1]["real"] == self.__node_clusters[j+1]["real"]:
                        tfpn[0][0] += 1 #true positive
                    else:
                        tfpn[0][1] += 1 #false negative
                else:
                    if self.__node_clusters[i+1]["real"] == self.__node_clusters[j+1]["real"]:
                        tfpn[1][0] += 1 #false positive
                    else:
                        tfpn[1][1] += 1 #true negative

        return (tfpn[0][0]+tfpn[1][1])/(tfpn[0][0]+tfpn[0][1]+tfpn[1][0]+tfpn[1][1])

def dump_cluster_coverage(clusters_num, clusters_groups):
    coverage_dict = {}

    for g in clusters_groups:
        coverage = (len(clusters_groups[g])/clusters_num)*100
        coverage_dict[g] = coverage
        print "cluster: {}={}".format(g, round(coverage, 4))

    return coverage_dict

def dump_max_core_coverage(alg, coverage_dict):
    unique_clusters = set(alg)
    clusters_list = map(str, unique_clusters)
    print "max_core-list-clusters: {}".format(','.join(clusters_list))

    max_core_coverage = 0
    for cls in unique_clusters:
        max_core_coverage += coverage_dict[cls]

    print "max_core_cluster_coverage: {}".format(max_core_coverage)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    
    parser.add_argument("-r", "--real-clusters", dest="realclustersfile", type=str,
                        help="File containing the real clusters.")
    parser.add_argument("-a", "--alg-clusters", dest="algclustersfile", type=str,
                        help="File containing the clusters found by cluster algorithms.", default=None)
    #parser.add_argument("-n", "--numpy-cython", dest="use_numpy", action="store_true", default=False,
    #                    help="Use the cython/numpy version of ri and rmi.")
    parser.add_argument("-c", "--cores", dest="cores_file", type=str,
                        help="File containing the cores, if passed then metrics for \
                        the max_core will be calculated")
    parser.add_argument("-d", "--do-coverage", dest="coverage", action="store_true", default=False,
                        help="Dump cluster coverage info.")
    args = parser.parse_args()

    RI = 0
    NMI = 0

    if args.coverage:
        clusters, clusters_groups = load_community_groups(args.algclustersfile)
        if clusters.shape[0] == 0:
            sys.exit(0)

        coverage_dict = dump_cluster_coverage(len(clusters), clusters_groups)
        selected_indexes = load_cores_indexes(args.cores_file)
        #real = load_community_file_cores(args.realclustersfile, selected_indexes)
        alg = load_community_file_cores(args.algclustersfile, selected_indexes)
        print "max_core_nodes: {}".format(len(alg))
        dump_max_core_coverage(alg, coverage_dict)
        sys.exit(0)

    # if not args.use_numpy:
    #     metrics = Evaluation()
    #     metrics.LoadFile(args.realclustersfile, "real")
    #     metrics.LoadFile(args.algclustersfile, "alg")
    #     RI = metrics.RI()
    #     NMI = metrics.NMI()
    # else:
    extra=""
    if args.cores_file:
        extra="max_core-"
        selected_indexes = load_cores_indexes(args.cores_file)
        #print selected_indexes
        real = load_community_file_cores(args.realclustersfile, selected_indexes)
        alg = load_community_file_cores(args.algclustersfile, selected_indexes)

        # print the number of clusters in maxcore
        print "max_core-clusters: {}".format(len(set(alg)))
    else:
        real = load_community_file(args.realclustersfile)
        alg = load_community_file(args.algclustersfile)
            
        # stuff from fast metrics
        RI = ri(real, alg)
        NMI = nmi(real, alg)

    print "{}RI: {}".format(extra,RI)
    print "{}NMI: {}".format(extra,NMI)
