#ifndef LEADINGEIGENVECTOR
#define LEADINGEIGENVECTOR

#include "Cluster.h"
#include "Graph.h"
#include "IGraphShared.h"

class LeadingEigenvector : public Cluster, public IGraphShared {
public:
    void LoadFile(std::string filename);
    void Cluster();
    std::vector<int> CCluster(std::map<int, std::vector<int>>& grapht,
                                       std::map<std::string, std::string>& configt);
private:
    Graph graph;
};


#endif // LEADINGEIGENVECTOR

