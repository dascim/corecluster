#include "Spinglass.h"
#include <iostream>
#include <tuple>
#include <set>
#include <limits>

void Spinglass::LoadFile(std::string filename){
    graph.Load(filename);
}

// LoadCommunity must be called first
int Spinglass::GetGroundK(){
    if (real_clusters.empty())
        return -1;

    std::set<int> uniq_cls;

    for (auto& iter : real_clusters){
        uniq_cls.insert(iter.second);
    }

    return uniq_cls.size();
}

void Spinglass::LoadGroundTruth(
        std::map<int, int> &real_clusters, std::vector<int> &core_nodes){

    std::set<int> uniq_cls;

    for (auto& node_id : core_nodes){
        uniq_cls.insert(real_clusters[node_id]);
    }

    current_truth_k = uniq_cls.size();
}

std::vector<int> Spinglass::CCluster(std::map<int, std::vector<int> > &grapht,
                                        std::map<std::string, std::string> &configt){

    std::vector<int> clusters(grapht.size());

    if (current_truth_k <= 1)
        return clusters;

    int spins = current_truth_k;
    igraph_t g;
    InitializeIGraphObj(g, grapht, true);

    //for some reason if the graph is weakly connected then
    //the spinglass algorithm does not work and throws an error
    //so for the moment to overcome this problem if it is
    //weakly connected we just return all the nodes in the
    //same cluster which is bad but it should make it work.
    igraph_bool_t conn;
    igraph_is_connected(&g, &conn, IGRAPH_WEAK);
    if (!conn)
        return clusters;

    igraph_vector_t membership;
    long int no_of_nodes = igraph_vcount(&g);

    igraph_vector_init(&membership, 0);

    igraph_community_spinglass(&g,
               /*weights */ 0,
               /*modularity */ 0,
               /*temperature */ 0,
               &membership,
               /*csize */ 0,
               spins,
               /*parupdate */ 1,
               /*starttemp */ 1.0,
               /*stoptemp */ 0.01,
               /*coolfact */ 0.99,
               /*update_rule */ IGRAPH_SPINCOMM_UPDATE_SIMPLE,
               /*gamma */ 1.0,
               /* the rest is for the NegSpin implementation */
               /*implementation */ IGRAPH_SPINCOMM_IMP_ORIG,
               /*igraph_matrix_t *adhesion, */
               /*igraph_matrix_t *normalised_adhesion, */
               /*igraph_real_t *polarization, */
               /*gamma_minus */ 0);

    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        clusters[i] = VECTOR(membership)[i];
    }

    igraph_vector_destroy(&membership);
    igraph_destroy(&g);

    return clusters;
}

void Spinglass::Cluster(){

    igraph_t g;
    std::map<int, std::vector<int>> graphmap = graph.GetGraph();
    InitializeIGraphObj(g, graphmap, false);
    long int no_of_nodes = igraph_vcount(&g);

    //for some reason if the graph is weakly connected then
    //the spinglass algorithm does not work and throws an error
    //so for the moment to overcome this problem if it is
    //weakly connected we just return all the nodes in the
    //same cluster which is bad but it should make it work.
    igraph_bool_t conn;
    igraph_is_connected(&g, &conn, IGRAPH_WEAK);
    if (!conn){
        for (long int i = 0; i < no_of_nodes; i++){
            std::cout << i+1 << " " << 0 << std::endl;
        }
        return;
    }

    int spins = GetGroundK();
    igraph_vector_t membership;


    igraph_vector_init(&membership, 0);

    igraph_community_spinglass(&g,
               /*weights */ 0,
               /*modularity */ 0,
               /*temperature */ 0,
               &membership,
               /*csize */ 0,
               spins,
               /*parupdate */ 1,
               /*starttemp */ 1.0,
               /*stoptemp */ 0.01,
               /*coolfact */ 0.99,
               /*update_rule */ IGRAPH_SPINCOMM_UPDATE_SIMPLE,
               /*gamma */ 1.0,
               /* the rest is for the NegSpin implementation */
               /*implementation */ IGRAPH_SPINCOMM_IMP_ORIG,
               /*igraph_matrix_t *adhesion, */
               /*igraph_matrix_t *normalised_adhesion, */
               /*igraph_real_t *polarization, */
               /*gamma_minus */ 0);

    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        std::cout << i+1 << " " << VECTOR(membership)[i] << std::endl;
    }

    igraph_vector_destroy(&membership);
    igraph_destroy(&g);
}


