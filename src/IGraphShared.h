#ifndef IGRAPHSHARED
#define IGRAPHSHARED

#include <igraph/igraph.h>
#include <vector>
#include <map>

class IGraphShared {
protected:
    void InitializeIGraphObj(igraph_t &g, std::map<int, std::vector<int>> &graphmap, bool core_mode);
};

#endif // IGRAPHSHARED

