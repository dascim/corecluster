#include <set>
#include <limits>
#include "IGraphShared.h"

void IGraphShared::InitializeIGraphObj(igraph_t &g,
                        std::map<int,std::vector<int>> &graphmap, bool core_mode){

    std::set<std::tuple<int, int>> node_pairs;
    int first_node, second_node;

    //keep only the unique node edges
    for (auto &iter : graphmap){
        for (auto &neighbor: iter.second){
            if (iter.first < neighbor){
                first_node = iter.first;
                second_node = neighbor;
            }
            else{
                first_node = neighbor;
                second_node = iter.first;
            }

            //in the normal cluster run node indexes start from 1
            //but in core cluster mode start with 0
            if (!core_mode){
                first_node -= 1;
                second_node -= 1;
            }

            node_pairs.insert(std::make_tuple(first_node, second_node));
        }
    }

    igraph_vector_t v;
    std::vector<igraph_real_t> graph_edges_v;
    //put the edges in a sequence into
    for (auto &pair : node_pairs){
        graph_edges_v.push_back(std::get<0>(pair));
        graph_edges_v.push_back(std::get<1>(pair));
    }

    igraph_vector_view(&v, graph_edges_v.data(), graph_edges_v.size());
    igraph_create(&g, &v, 0, IGRAPH_UNDIRECTED);
}

