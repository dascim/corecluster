#include "LeadingEigenvector.h"
#include <iostream>
#include <tuple>
#include <set>
#include <limits>

void LeadingEigenvector::LoadFile(std::string filename){
    graph.Load(filename);
}

std::vector<int> LeadingEigenvector::CCluster(std::map<int, std::vector<int> > &grapht,
                                        std::map<std::string, std::string> &configt){
    igraph_t g;
    InitializeIGraphObj(g, grapht, true);

    igraph_vector_t membership;
    long int no_of_nodes = igraph_vcount(&g);
    igraph_vector_init(&membership, 0);

    int steps = no_of_nodes;
    igraph_arpack_options_t options;
    igraph_arpack_options_init(&options);

    //call the actual clustering function
    igraph_community_leading_eigenvector(&g,
            /*weights */ 0,
            /*merges */ 0,
            &membership,
            steps,
            /*options */ &options,
            /*modularity */ 0,
            /*start */ 0,
            /*eigenvalues */ 0,
            /*eigenvectors */ 0,
            /*history */ 0,
            /*callback */ 0,
            /*callback_extra */ 0);

    std::vector<int> clusters(no_of_nodes);
    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        clusters[i] = VECTOR(membership)[i];
    }

    igraph_vector_destroy(&membership);
    igraph_destroy(&g);

    return clusters;
}

void LeadingEigenvector::Cluster(){
    igraph_t g;
    std::map<int, std::vector<int>> graphmap = graph.GetGraph();
    InitializeIGraphObj(g, graphmap, false);

    igraph_vector_t membership;
    long int no_of_nodes = igraph_vcount(&g);
    igraph_vector_init(&membership, 0);

    int steps = no_of_nodes;
    igraph_arpack_options_t options;
    igraph_arpack_options_init(&options);

    igraph_community_leading_eigenvector(&g,
            /*weights */ 0,
            /*merges */ 0,
            &membership,
            steps,
            /*options */ &options,
            /*modularity */ 0,
            /*start */ 0,
            /*eigenvalues */ 0,
            /*eigenvectors */ 0,
            /*history */ 0,
            /*callback */ 0,
            /*callback_extra */ 0);

    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        std::cout << i+1 << " " << VECTOR(membership)[i] << std::endl;
    }

    igraph_vector_destroy(&membership);
    igraph_destroy(&g);
}


