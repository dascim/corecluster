#ifndef WALKTRAPUTIL
#define WALKTRAPUTIL

#include <string>

namespace WT {
    void StandardClusterRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartWalkTrap(int argc, char * argv[]);
}

#endif // WALKTRAPUTIL

