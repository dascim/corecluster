#ifndef COMMUNITYLOADER
#define COMMUNITYLOADER

#include <map>
#include <string>

class CommunityLoader {
public:
    void LoadCommunity(std::string filename);
protected:
    std::map<int, int> real_clusters;
};

#endif // COMMUNITYLOADER

