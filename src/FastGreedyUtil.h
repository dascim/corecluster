#ifndef FASTGREEDYUTIL
#define FASTGREEDYUTIL

#include <string>

namespace FG {
    void StandardClusterRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartFastGreedy(int argc, char * argv[]);
}
#endif // FASTGREEDYUTIL

