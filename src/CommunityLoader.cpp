#include "CommunityLoader.h"
#include <fstream>
#include <iostream>

void CommunityLoader::LoadCommunity(std::string filename){
    std::ifstream fin(filename);
    int node_id, cluster_id;

    while (fin >> node_id >> cluster_id){
        real_clusters[node_id] = cluster_id;
    }
}
