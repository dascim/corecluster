#ifndef CONFIGLOADER_H
#define CONFIGLOADER_H

#include <map>

class ConfigLoader {
public:
    void LoadConfig(std::string filename);
    // #TODO:for debug, to be removed
    void print_config();
protected:
    std::map<std::string, std::string> config;

};

#endif // CONFIGLOADER_H
