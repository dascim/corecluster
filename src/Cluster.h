#ifndef CLUSTER_H
#define CLUSTER_H

#include <map>
#include <string>
#include <vector>

class Cluster {
public:
    /* CCluster (core cluster)
     * graph is a map with node_id: vector with neighbours
     * config: map with configuration settings in case a config
     *  is provided the kCoreCluster class
     */
    Cluster(){}
    virtual ~Cluster(){}
    virtual std::vector<int> CCluster(std::map<int, std::vector<int>>& grapht,
                                       std::map<std::string, std::string>& configt) = 0;
    virtual void LoadGroundTruth(std::map<int, int> & /*real_clusters*/, std::vector<int> & /*core_nodes*/){};
};

#endif // CLUSTER_H
