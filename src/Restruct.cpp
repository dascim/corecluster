#include "Restruct.h"

#include <limits>
#include <math.h>
#include <iostream>

void Restruct::Select(std::map<int, int>& node_clusters, int max_cluster,
                      std::vector<int>& unclustered_nodes, std::map<int, std::vector<int>>& grapht){

     // #TODO compute the bin, check if we really need this here(it is for the special case handle)
    std::vector<int> bin = GetBin(max_cluster, node_clusters);

    // node_clusters and unclustered_nodes change
    Optimize(node_clusters, max_cluster, unclustered_nodes, grapht);

    bool neighbor_in_cluster;
    // temporarily store all the node ids without neighbor in cluster here
    // use set for fast look up
    std::set<int> not_neighbor_clusters;

    for (auto& node_id : unclustered_nodes){
        std::map<int,int>::iterator icluster;
        std::vector<int> count(max_cluster+1);
        neighbor_in_cluster = false;

        for (auto& neighbor_id : grapht[node_id]){
            //return  1(exists) or 0 (does not exist)
            icluster = node_clusters.find(neighbor_id);

            if (icluster != node_clusters.end()){
                count[icluster->second] += 1;

                if (count[icluster->second] > 1){
                    neighbor_in_cluster = true;
                    break;
                }
            }
        }

        if (neighbor_in_cluster){
            neighbor_clusters.push_back(node_id);
        }
        else{
            not_neighbor_clusters.insert(node_id);
        }
    }

    bool is_connected_together;

    for (auto& node_id : not_neighbor_clusters){
        is_connected_together = false;

        for (auto& neighbor_id : grapht[node_id]){
            if (not_neighbor_clusters.count(neighbor_id)){
                is_connected_together = true;
                break;
            }
        }

        if (is_connected_together){
            // store the node with and its position
            // we do that in order to avoid an extra O(n) pass
            // when we pass the nodes to the cluster_alg
            together_connected_pos[node_id] = together_connected.size();
            //# TODO check to give position hint to insert function
            //  because usually the nodes are given in sorted order
            together_connected.push_back(node_id);
        }
        else
            not_together_connected.push_back(node_id);
    }

    // special case to handle unclustered data
    if (together_connected.size() == 0 &&
            (neighbor_clusters.size() > 0 || not_together_connected.size() > 0))
        SpecialHandleUnclusteredNodes(node_clusters, max_cluster, grapht, bin);

}

void Restruct::Join(std::map<int, int> &node_clusters,
                    int max_cluster, std::map<int, std::vector<int> > &grapht){
    //here we break the function into two parts (it's easier to debug and read the code)
    HandleNeighborCluster(node_clusters, max_cluster, grapht);

    std::vector<int> bin = GetBin(max_cluster, node_clusters);
    HandleNotTogetherConnected(node_clusters, max_cluster, grapht, bin);

}

void Restruct::HandleNeighborCluster(std::map<int, int> &node_clusters,
                                     int max_cluster, std::map<int, std::vector<int> > &grapht){

    size_t presize = neighbor_clusters.size() - 1;
    std::map<int,int>::iterator icluster;
    int max_linkage = -1, scluster = -1;
    bool again;

    while (neighbor_clusters.size() > 0){
        //if nothing change in the previous loop run assign (dosom3 java code)
        if (presize == neighbor_clusters.size()){
            Assign(node_clusters, max_cluster, grapht);
        }

        presize = neighbor_clusters.size();
        //nodes remaining unassigned
        std::vector<int> nremain;

        for (auto& node_id : neighbor_clusters){
            std::vector<int> linkage(max_cluster+1);
            again = false;

            for (auto& neighbor_id : grapht[node_id]){
                icluster = node_clusters.find(neighbor_id);

                if (icluster != node_clusters.end()){
                    linkage[icluster->second]++;
                }
                else{
                    again = true;
                    break;
                }
            }//endfor

            if(!again){
                max_linkage = -1;
                scluster = -1;

                for (size_t l = 0; l < linkage.size(); l++){
                    if (linkage[l] > max_linkage){
                        max_linkage = linkage[l];
                        scluster = l;
                    }
                }
                node_clusters[node_id] = scluster;
            }
            else{
                nremain.push_back(node_id);
            }
        }// endfor

        // #TODO check if we really need to clear the container here
        neighbor_clusters.clear();

        if (nremain.size() > 0){
            neighbor_clusters = nremain;
        }

    }// endwhile
}

void Restruct::HandleNotTogetherConnected(std::map<int, int> &node_clusters,
                                          int max_cluster, std::map<int, std::vector<int> > &grapht, std::vector<int>& bin){
    // #TODO check again this is almost identical with the assign function, thing a way to wrap it

    for (auto& node_id : not_together_connected){

        double max_linkage = std::numeric_limits<int>::min();
        double clinkage = -1;
        // node position, selected cluster
        int snode_id, scluster = -1;

        std::map<int,int>::iterator icluster;
        std::vector<double> linkage(max_cluster+1);

        for (auto& neighbor_id : grapht[node_id]){
            icluster = node_clusters.find(neighbor_id);

            // if found
            if (icluster != node_clusters.end()){
                linkage[icluster->second]++;
            }
        }

        for (size_t l=0; l < linkage.size(); l++){
            // to avoid having zero in a division
            if (linkage[l] == 0 || bin[l] == 0)
                continue;

            linkage[l] = linkage[l] / log(bin[l]);

            clinkage = linkage[l];
            if (clinkage > max_linkage){
                max_linkage = clinkage;
                scluster = l;

                //selected node_id
                snode_id = node_id;
            }
        }

        if (scluster == -1){
            std::cerr << "DAMN ERRAR HandleNotTogetherConnected" << std::endl;
            exit(1);
        }

        node_clusters[snode_id] = scluster;
        bin[scluster]++;
    }//endfor

}

std::vector<int> Restruct::GetBin(int max_cluster, std::map<int, int> &node_clusters){
    std::vector<int> bin(max_cluster+1);

    for (auto& iter : node_clusters){
        bin[iter.second]++;
    }

    return bin;
}

void Restruct::SpecialHandleUnclusteredNodes(std::map<int, int> &node_clusters,
                     int max_cluster, std::map<int, std::vector<int> > &grapht, std::vector<int>& bin){

    while(neighbor_clusters.size() > 0){
        Assign(node_clusters, max_cluster, grapht);
    }


    HandleNotTogetherConnected(node_clusters, max_cluster, grapht, bin);
    not_together_connected.clear();
    //neighbor_clusters should be empty so not reason to clear it
}

// this is the dosom3 function of the java code
void Restruct::Assign(std::map<int, int> &node_clusters,
                      int max_cluster, std::map<int, std::vector<int> > &grapht){

    // #TODO check to wrap it with GetBin
    std::vector<int> bin(max_cluster+1);
    for (auto& iter : node_clusters){
        bin[iter.second]++;
    }

    //#TODO check why if working with initial minvalue double
    double max_linkage = std::numeric_limits<int>::min();
    double clinkage = -1;
    // node position, selected cluster
    int node_id, sposition, scluster = -1;

    for (size_t i = 0; i < neighbor_clusters.size(); i++){
        node_id = neighbor_clusters[i];

        std::map<int,int>::iterator icluster;
        std::vector<double> linkage(max_cluster+1);

        for (auto& neighbor_id : grapht[node_id]){
            icluster = node_clusters.find(neighbor_id);

            // if found
            if (icluster != node_clusters.end()){
                linkage[icluster->second]++;
            }
        }

        for (size_t l=0; l < linkage.size(); l++){
            // to avoid having zero in a division
            if (linkage[l] == 0 || bin[l] == 0)
                continue;

            linkage[l] = linkage[l] / log(bin[l]);

            clinkage = linkage[l];
            if (clinkage > max_linkage){
                max_linkage = clinkage;
                scluster = l;

                //selected node_id
                sposition = i;
            }
        }
    }
    if (scluster == -1){
        std::cerr << "DAMN ERRAR Assign" << std::endl;
        exit(1);
    }

    // assign the selected node_id to the selected cluster
    node_clusters[neighbor_clusters[sposition]] = scluster;
    RemoveFromVector(neighbor_clusters, sposition);

}

void Restruct::RemoveFromVector(std::vector<int>& vec, int index){
    // put the last element in the index we want to remove
    vec[index] = vec[vec.size() - 1];
    // remove the last element, complexity : constant
    vec.pop_back();
}

// this function apply the Pa,b property from the paper
// the unclustered nodes changes
void Restruct::Optimize(std::map<int, int>& node_clusters,
                        int max_cluster, std::vector<int>& unclustered_nodes, std::map<int, std::vector<int>>& grapht){

    int cluster_changes = 1;
    bool all_neighbors_in_clusters;
    std::map<int,int>::iterator ncluster;

    do {
        cluster_changes = 0;
        std::vector<int> remaining_nodes;

        for (auto& node_id : unclustered_nodes){
            std::vector<int> cluster_counts(max_cluster+1);
            all_neighbors_in_clusters = true;

            // for each neighbor node id
            for (auto& nnode_id : grapht[node_id]){
                // we check in which cluster the current node's neighbors exist
                ncluster = node_clusters.find(nnode_id);
                // if the node id key was found
                if (ncluster != node_clusters.end()){
                    // ncluster.second contains the cluster id
                    cluster_counts[ncluster->second]++;
                }
                else
                    all_neighbors_in_clusters = false;
            }

            int count_temp, max_cluster_count = 0, most_cluster_id = 0;

            //find the the cluster where the most of the nodes neighbor belong
            for (size_t i = 0; i < cluster_counts.size(); i++){
                count_temp = cluster_counts[i];
                if ( count_temp > max_cluster_count ){
                    max_cluster_count = count_temp;
                    most_cluster_id = i;
                }
            }

            //now apply the Pa,b property, perc is the a property of the paper
            int neighbors_size = grapht[node_id].size();
            double perc = static_cast<double>(max_cluster_count)/static_cast<double>(neighbors_size);

            if (all_neighbors_in_clusters || (perc >= 0.8 && neighbors_size >= 5)){
                // if true add the node_id in the selected cluster
                node_clusters[node_id] = most_cluster_id;
                cluster_changes++;
            }
            else
                remaining_nodes.push_back(node_id);
        }

        unclustered_nodes = remaining_nodes;

    // even if one new node was added to an existing cluster
    // execute again the Optimize loop
    } while(cluster_changes > 0);
}

std::vector<int> *Restruct::GetRemainingNodes(){
    return &together_connected;
}

std::map<int, int>* Restruct::GetRemainingPositions(){
    return &together_connected_pos;
}

bool Restruct::HasRemainingNodes(){
    return !together_connected.empty();
}

int Restruct::GetRemainingNodesNum(){
    return together_connected.size();
}


