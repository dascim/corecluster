#include "WalkTrap.h"
#include <iostream>
#include <tuple>
#include <set>
#include <limits>

void WalkTrap::LoadFile(std::string filename){
    graph.Load(filename);
}

std::vector<int> WalkTrap::CCluster(std::map<int, std::vector<int> > &grapht,
                                        std::map<std::string, std::string> &configt){
    igraph_t g;
    InitializeIGraphObj(g, grapht, true);

    igraph_vector_t membership;
    igraph_vector_t modularity;
    igraph_matrix_t merges;
    long int no_of_nodes = igraph_vcount(&g);

    //proposed from the algorithm's paper
    //TODO test using using maybe log10
    int steps = static_cast<int>(log(no_of_nodes));
    if (steps == 0){
        steps = 1;
    }

    igraph_vector_init(&membership, 0);
    igraph_vector_init(&modularity, 0);
    igraph_matrix_init(&merges, 0, 0);

    igraph_community_walktrap(&g,
              /*weights */ 0,
              steps,
              &merges,
              &modularity,
              &membership);

    std::vector<int> clusters(no_of_nodes);
    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        clusters[i] = VECTOR(membership)[i];
    }

    igraph_vector_destroy(&membership);
    igraph_vector_destroy(&modularity);
    igraph_matrix_destroy(&merges);
    igraph_destroy(&g);

    return clusters;
}

void WalkTrap::Cluster(){
    igraph_t g;
    std::map<int, std::vector<int>> graphmap = graph.GetGraph();
    InitializeIGraphObj(g, graphmap, false);

    igraph_vector_t membership;
    igraph_vector_t modularity;
    igraph_matrix_t merges;
    long int no_of_nodes = igraph_vcount(&g);
    //proposed from the algorithm's paper
    //TODO test using using maybe log10
    int steps = static_cast<int>(log(no_of_nodes));
    if (steps == 0){
        steps = 1;
    }

    igraph_vector_init(&membership, 0);
    igraph_vector_init(&modularity, 0);
    igraph_matrix_init(&merges, 0, 0);

    igraph_community_walktrap(&g,
              /*weights */ 0,
              steps,
              &merges,
              &modularity,
              &membership);

    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        std::cout << i+1 << " " << VECTOR(membership)[i] << std::endl;
    }

    igraph_vector_destroy(&membership);
    igraph_vector_destroy(&modularity);
    igraph_matrix_destroy(&merges);
    igraph_destroy(&g);
}



