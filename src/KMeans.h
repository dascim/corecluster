#ifndef KMEANS_H
#define KMEANS_H

#include <vector>
#include <shogun/lib/SGMatrix.h>

class KMeans {
public:
    //number of features, number of cluster, number of iterations
    KMeans(int num_of_features, int dim, int num_of_cls, int num_of_itr);
    std::vector<int> Cluster(shogun::SGMatrix<float64_t>& mx);
    std::vector<int> Cluster(shogun::SGMatrix<float64_t>& mx, int kf);

private:
    int k;
    int max_iters;
    int dimensions;
    float64_t THRE;
    std::vector<int> clusters;
    std::vector< std::vector<float64_t> > centers;
    std::vector< std::vector<float64_t> > PPInit(shogun::SGMatrix<float64_t>& mx, int kf);
    std::vector< std::vector<float64_t> > Get2DVector(int x, int y);
};



#endif // KMEANS_H
