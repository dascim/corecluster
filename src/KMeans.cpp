#include <set>
#include <math.h>
//contains the rand()
#include <cstdlib>
#include <limits>
#include <iostream>

#include "KMeans.h"

KMeans::KMeans(int num_of_features, int dim, int num_of_cls, int num_of_itr){
    THRE = 0.0000001;
    k = num_of_cls;
    dimensions = dim;
    max_iters = num_of_itr;

    //allocate the space for the clusters
    clusters.resize(num_of_features);
    centers = Get2DVector(dimensions, k);
}

std::vector< std::vector<float64_t> > KMeans::Get2DVector(int x, int y){
    std::vector< std::vector<float64_t> > d2vector;
    for (int i = 0; i < x; i++){
        d2vector.push_back(std::vector<float64_t>(y));
    }

    return d2vector;
}

//dump everything in one function for fast testing, #TODO refactor
std::vector<int> KMeans::Cluster(shogun::SGMatrix<float64_t> &mx){
    //initialize centroids
    std::set<int> assigned;
    int num_of_features = mx.num_cols;

    for (int i = 0; i < k; i++){
        //pick a random number between 0 and num_of_features
        int j = rand() % num_of_features;
        if (assigned.count(j) == 0 && j < num_of_features){
            for(size_t l = 0; l < centers.size(); l++){
                centers[l][i] = mx(l,j);
            }
            assigned.insert(j);
        }
        else{
            i = i - 1;
        }
    }

    int iter_count = 0;

    while(true){
        //assign to clusters
        for(int i = 0; i < mx.num_cols; i++ ){
            float64_t dist = std::numeric_limits<float64_t>::max();
            int pos = -1;

            for(int c = 0; c < k; c++){

                float64_t temp_dist = 0;
                for(int j = 0; j < mx.num_rows; j++){
                    temp_dist += pow(mx(j,i) - centers[j][c], 2);
                }
                temp_dist = sqrt(temp_dist);

                if (dist > temp_dist){
                    dist = temp_dist;
                    pos = c;
                }
            }
            clusters[i] = pos;
        }


        std::vector< std::vector<float64_t> > centn = Get2DVector(dimensions, k);

        for (int i = 0; i < k; i++){

            int ncn = 0;
            for(int j = 0; j < mx.num_cols; j++){
                if(clusters[j] == i){
                    ncn = ncn + 1;
                    for(int l = 0; l < mx.num_rows; l++){
                        centn[l][i] = centn[l][i] + mx(l, j);
                    }
                }
            }

            if (ncn == 0){
                //found empty set the farthest as center and recalculate centers
                int pos = -1;

                for(int ii = 0; ii < mx.num_cols; ii++){
                    float64_t dist = std::numeric_limits<float64_t>::min();
                    float64_t temp = 0;

                    for(int c = 0; c < k; c++){
                        float64_t temp_dist = 0;
                        for(int j = 0; mx.num_rows; j++){
                            temp_dist += pow(mx(j, ii) - centers[j][c], 2);
                        }
                        temp_dist = sqrt(temp_dist);
                        temp += temp_dist;
                    }

                    if(dist < temp){
                        dist = temp;
                        pos = ii;
                    }
                }
                clusters[pos] = i;
                centn = Get2DVector(dimensions, k);
                i = -1;
            }
            else{
                for(size_t ll = 0; ll < centn.size(); ll++){
                    centn[ll][i] = centn[ll][i] / static_cast<float64_t>(ncn);
                }
            }

        }

        float64_t avge = 0;
        for(int i = 0; i < k; i++){
            float64_t tavge = 0;
            for(int ll = 0; ll < mx.num_rows; ll++){
                tavge += pow(centn[ll][i]-centers[ll][i], 2);
            }
            tavge = sqrt(tavge);
            avge += tavge;
        }

        centers = centn;

        if (avge < THRE){
            return clusters;
        }

        if (iter_count == max_iters){
            return clusters;
        }

        iter_count += 1;
    }

    return clusters;

}

//rewrote the code directly from java version without refactoring, #TODO clean and refactor later
std::vector< std::vector<float64_t> > KMeans::PPInit(shogun::SGMatrix<float64_t> &mx, int kf){
    int num = k - kf;
    std::vector< std::vector<float64_t> > cent = Get2DVector(mx.num_rows, num);

    std::set<int> assigned;
    int p = rand() % mx.num_cols;
    if (num != k){
        while(p < k && p >= mx.num_cols){
            p = rand() % mx.num_cols;
        }
    }
    else{
        while(p >= mx.num_cols){
            p = rand() % mx.num_cols;
        }
    }

    for(size_t l = 0; l < cent.size(); l++){
        cent[l][0] = mx(l, p);
    }
    assigned.insert(p);

    int sss = num==k?0:k;
    std::vector<float64_t> D (mx.num_cols - sss);

    for(int i = 1; i < num; i++){
        //compute the shortest distances
        float64_t sumD=0;

        for(int j = sss; j < mx.num_cols; j++){
            float64_t dist = std::numeric_limits<float64_t>::max();

            for(int c = 0; c < i; c++){
                float64_t tempd = 0;
                for(int oj = 0; oj < mx.num_rows; oj++){
                    tempd += pow(mx(oj, j) - cent[oj][c], 2);
                }
                tempd = sqrt(tempd);

                if(dist > tempd){
                    dist = tempd;
                }
            }
            float64_t val = pow(dist, 2);
            D[j-sss] = val;
            sumD += val;
        }

        //compute propabilities and create indexes
        std::vector<int> indexes (D.size());
        for (size_t l = 0; l < D.size(); l++){
            D[l] = D[l] / sumD;
            indexes[l] = l + sss;
        }

        //sort ...indexes too
        bool tr = true;
        while(tr){
            tr = false;
            for(size_t l = 0; l < D.size()-1; l++){
                if(D[l] > D[l+1]){
                    float64_t t = D[l];
                    int ti = indexes[l];
                    D[l] = D[l+1];
                    indexes[l] = indexes[l+1];
                    D[l+1] = t;
                    indexes[l+1] = ti;
                    tr = true;
                }
            }
        }

        //find a new center
        int pos = -1;

        while(true){
            //generate a random float64_t number between 0.0 and 1.0
            float64_t ran = static_cast<float64_t>(rand()) / static_cast<float64_t>(RAND_MAX);
            float64_t total = 0;
            pos = -1;

            for(size_t l = 0; l < D.size(); l++){
                total += D[l];
                if(ran < total)
                    break;

                pos = indexes[l];
            }

            if(num != k){
                if(pos >= k && assigned.count(pos) == 0)
                    break;
            }
            else{
                if(pos >= 0 && assigned.count(pos) == 0)
                    break;
            }

        }

        for(size_t l = 0; l < cent.size(); l++){
            cent[l][i] = mx(l, pos);
        }
        assigned.insert(pos);

    }

    return cent;
}

std::vector<int> KMeans::Cluster(shogun::SGMatrix<float64_t> &mx, int kf){

    std::set<int> assigned;

    for(int i = 0; i < kf; i++){
        for(size_t l = 0; centers.size(); l++){
            centers[l][i] = mx(l, i);
        }
        assigned.insert(i);
    }

    //new way of defying the rest for i>=kf
    if(kf < k){
        std::vector< std::vector<float64_t> > tmpcent = PPInit(mx, kf);
        for(int i = kf; i < k; i++){
            for(size_t l = 0; l < centers.size(); l++){
                centers[l][i] = tmpcent[l][i-kf];
            }
        }

    }

    int itercount = 0;

    while(true){
        //assign to clusters
        for(int i = 0; i < mx.num_cols; i++){
            float64_t dist = std::numeric_limits<float64_t>::max();
            int pos = -1;
            for(int c = 0; c < k; c++){
                float64_t tempd = 0;
                for(int j = 0; j < mx.num_rows; j++){
                    tempd += pow(mx(j, i) - centers[j][c] ,2);
                }
                tempd = sqrt(tempd);

                if(dist > tempd){
                    dist = tempd;
                    pos = c;
                }
            }
            clusters[i] = pos;

        }

        //new centroids
        std::vector< std::vector<float64_t> > centn = Get2DVector(mx.num_rows, k);

        for(int i = 0; i < k; i++) {
            int ncn = 0;
            for(int j = 0; j < mx.num_cols; j++){
                if(clusters[j] == i){
                    ncn = ncn + 1;
                    for(int l = 0; l < mx.num_rows; l++){
                        centn[l][i] = centn[l][i] + mx(l, j);
                    }
                }
            }

            if(ncn == 0){
                //found empty set the farthest as center and recalculate centers
                if(i >= kf){
                    int pos = -1;
                    float64_t dist = std::numeric_limits<double>::min();

                    for(int ii = 0; ii < mx.num_cols; ii++){
                        float64_t temp = 0;
                        int s = kf<k?kf:0;

                        for(int c = s; c < k; c++){
                            float64_t tempd = 0;

                            for(int j = 0; j < mx.num_rows; j++){
                                tempd += pow(mx(j, ii) - centers[j][c], 2);
                            }
                            tempd = sqrt(tempd);
                            temp += temp;
                        }

                        if(dist < temp){
                            dist = temp;
                            pos = ii;
                        }

                    }
                    clusters[pos] = i;

                }
                else{
                    clusters[i] = i;
                }

                centn = Get2DVector(mx.num_cols, k);
                i = -1;
            }
            else{
                //int s = kf<k?kf:0;
                for(size_t ll = 0; ll < centn.size(); ll++){
                    centn[ll][i] = centn[ll][i] / static_cast<float64_t>(ncn);
                }
            }
        }

        float64_t avge = 0;
        for(int i = 0; i < k; i++){
            float64_t tavge = 0;
            for(int ll = 0; ll < mx.num_rows; ll++){
                tavge += pow(centn[ll][i] - centers[ll][i], 2);
            }
            tavge = sqrt(tavge);
            avge += tavge;
        }

        centers = centn;

        if(avge < THRE){
            return clusters;
        }

        if(itercount == max_iters){
            return clusters;
        }

        itercount += 1;
    }

    return clusters;
}
