#include "ConfigLoader.h"

#include <sstream>
#include <iostream>
#include <fstream>

// used for trimming the config values
std::string trim(std::string const& str)
{
    std::size_t first = str.find_first_not_of(' ');
    std::size_t last  = str.find_last_not_of(' ');
    return str.substr(first, last-first+1);
}

// load the config file
void ConfigLoader::LoadConfig(std::string filename){
    std::ifstream configfile(filename);

    std::string line;
    while(std::getline(configfile, line)){
        // ignore the comments
        if ((line.find("#") == 0) || (line.empty()))
            continue;

        std::string token;
        std::istringstream sline( line );

        // read the first token which is the conf key(name)
        std::getline(sline, token, '=');
        std::string conf_key = trim(token);

        // read the second token which is the conf value
        std::getline(sline, token, '=');
        std::string conf_value = trim(token);


        // save the conf
        config[conf_key] = conf_value;
    }
    configfile.close();
}

// #TODO for debug, to be removed
void ConfigLoader::print_config(){
    for (auto& iter : config){
        std::cout << iter.first << " = " << iter.second << std::endl;
    }
}
