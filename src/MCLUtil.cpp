#include "CoreCluster.h"
#include "MCL.h"
#include "MCLUtil.h"

#include <iostream>
#include <math.h>
#include <ctime>
#include <chrono>
#include <fstream>

#include "GlobalTimer.h"

void MC::StandardMCLRun(std::string graphfile, std::string fileout){

    std::cerr << "#------------------------#" << std::endl;


    GT::timer.Start();

    MCL* cl = new MCL();

    cl->LoadFile(graphfile);
    std::vector<int> clusters =  cl->Cluster();

    delete cl;
    GT::timer.Stop();

    std::ofstream fout(fileout, std::ofstream::out);
    for (size_t i = 0; i < clusters.size(); i++){
        fout << i+1 << " " << clusters[i] << std::endl;
    }
    fout.close();

    std::cerr << "Elapsed time: " << GT::timer.GetElapsedTime() << std::endl;
}

void MC::CoreClusterRun(std::string graphfile){

    std::cerr << "#------------------------#" << std::endl;


    GT::timer.Start();

    MCL* cl = new MCL();
    CoreCluster* cc = new CoreCluster(cl);

    cc->Load(graphfile);

    //manually call core decomposition in order
    //to measure the time
    cc->CoreDecomposition();

    GT::timer.Stop();
    double core_decomp_time = GT::timer.GetElapsedTime();

    GT::timer.Start();
    cc->CCluster();

    delete cl;
    delete cc;

    // stop the timer
    GT::timer.Stop();
    double elapsed_time = GT::timer.GetElapsedTime();
    double core_alg_time = elapsed_time - core_decomp_time;

    /*std::ofstream fout(fileout, std::ofstream::out);
    for (auto &iter : clusters){
        fout << iter.first << " " << iter.second << std::endl;
    }
    fout.close();*/

    std::cerr << "Core decomposition time: " << core_decomp_time << std::endl
              << "Core algorithm time: " << core_alg_time << std::endl
            << "Elapsed time: " << elapsed_time << std::endl;

}

void MC::Usage(char prog[]){
    std::cerr << "Usage: " << prog << "mcl [c|s] input_graph_file" << std::endl
     << " pass 'c' for corecluster or 's' for standard cluster" << std::endl
     << " example: " << prog << "mcl c graph.txt" << std::endl;
}


int MC::StartMCL(int argc, char * argv[]){
    // #TODO add also the config file argument, we will propably need it
    // for the other cluster algorithms (other than spectral)

    // simple argument parsing:
    if (argc < 4){
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    char* mode = argv[2];

    if (strcmp(mode, "c") !=0 && strcmp(mode, "s") != 0){
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string graphfile = static_cast<std::string>(argv[3]);

    if (strcmp(mode, "s") == 0){
        std::string fileout = static_cast<std::string>(argv[4]);
        StandardMCLRun(graphfile, fileout);
    }
    else if(strcmp(mode, "c") == 0){
        CoreClusterRun(graphfile);
    }

    return 0;
}
