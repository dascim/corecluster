#include "EdgeBetweenness.h"
#include <iostream>
#include <tuple>
#include <set>
#include <limits>

void EdgeBetweenness::LoadFile(std::string filename){
    graph.Load(filename);
}

std::vector<int> EdgeBetweenness::CCluster(std::map<int, std::vector<int> > &grapht,
                                        std::map<std::string, std::string> &configt){
    igraph_t g;
    InitializeIGraphObj(g, grapht, true);

    igraph_vector_t membership;
    igraph_matrix_t merges;
    long int no_of_nodes = igraph_vcount(&g);

    igraph_vector_init(&membership, 0);
    igraph_matrix_init(&merges, 0, 0);

    //call the actual clustering function
    igraph_community_edge_betweenness(&g, /*edges */ 0,
                      /*edge betweenness*/ 0, &merges,
                      /*bridges */ 0 , /*modularity */ 0,
                      /*membership */ &membership,
                      IGRAPH_UNDIRECTED,
                      /*weights=*/ 0);

    std::vector<int> clusters(no_of_nodes);
    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        clusters[i] = VECTOR(membership)[i];
    }

    igraph_vector_destroy(&membership);
    igraph_matrix_destroy(&merges);
    igraph_destroy(&g);

    return clusters;
}

void EdgeBetweenness::Cluster(){
    igraph_t g;
    std::map<int, std::vector<int>> graphmap = graph.GetGraph();
    InitializeIGraphObj(g, graphmap, false);

    igraph_vector_t membership;
    igraph_matrix_t merges;
    long int no_of_nodes = igraph_vcount(&g);

    igraph_vector_init(&membership, 0);
    igraph_matrix_init(&merges, 0, 0);

    //call the actual clustering function
    igraph_community_edge_betweenness(&g, /*edges */ 0,
                      /*edge betweenness*/ 0, &merges,
                      /*bridges */ 0 ,/*modularity */ 0,
                      /*membership */ &membership,
                      IGRAPH_UNDIRECTED,
                      /*weights=*/ 0);

    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        std::cout << i+1 << " " << VECTOR(membership)[i] << std::endl;
    }

    igraph_vector_destroy(&membership);
    igraph_matrix_destroy(&merges);
    igraph_destroy(&g);
}
