#include "SpinglassUtil.h"
#include "CoreCluster.h"
#include "Spinglass.h"

#include <string.h>
#include <iostream>
#include <ctime>

void SG::StandardClusterRun(std::string graphfile, std::string communityfile){

    std::cerr << "#------------------------#" << std::endl;

    //start the timer
    std::clock_t begin = clock();

    Spinglass* cl = new Spinglass();

    cl->LoadCommunity(communityfile);
    cl->LoadFile(graphfile);
    cl->Cluster();

    delete cl;

    // stop the timer
    std::clock_t end = clock();

    double elapsed_time = static_cast<double>(end-begin) / CLOCKS_PER_SEC;
    std::cerr << "Elapsed time: " << elapsed_time << std::endl;
}

void SG::CoreClusterRun(std::string graphfile, std::string communityfile){

    std::cerr << "#------------------------#" << std::endl;

    Spinglass* cl = new Spinglass();
    //true -> enable ground truth mode
    CoreCluster* cc = new CoreCluster(cl, true);

    //start the timer
    std::clock_t begin = clock();

    cc->LoadCommunity(communityfile);
    cc->Load(graphfile);

    //manually call core decomposition in order
    //to measure the time
    cc->CoreDecomposition();

    std::clock_t end = clock();
    double core_decomp_time = static_cast<double>(end-begin) / CLOCKS_PER_SEC;


    //start again the timer
    begin = clock();

    cc->CCluster();

    delete cl;
    delete cc;

    // stop the timer
    end = clock();

    double core_alg_time = static_cast<double>(end-begin) / CLOCKS_PER_SEC;
    double elapsed_time = core_decomp_time + core_alg_time;

    std::cerr << "Core decomposition time: " << core_decomp_time << std::endl
              << "Core algorithm time: " << core_alg_time << std::endl
            << "Elapsed time: " << elapsed_time << std::endl;

}

void SG::Usage(char prog[]){
    std::cerr << "Usage: " << prog << " sg [c|s] input_graph_file community_file" << std::endl
     << " pass 'c' for corecluster or 's' for standard cluster" << std::endl
     << " example: " << prog << " sg c graph.txt community.txt" << std::endl;
}


int SG::StartSpinglass(int argc, char * argv[]){

    // simple argument parsing:
    if (argc != 5){
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    char* mode = argv[2];

    if (strcmp(mode, "c") !=0 && strcmp(mode, "s") != 0){
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string graphfile = static_cast<std::string>(argv[3]);
    std::string communityfile = static_cast<std::string>(argv[4]);

    if (strcmp(mode, "s") == 0){
        StandardClusterRun(graphfile, communityfile);
    }
    else if(strcmp(mode, "c") == 0){
        CoreClusterRun(graphfile, communityfile);
    }

    return 0;
}

