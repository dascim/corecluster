#ifndef METIS_H
#define METIS_H

#include <vector>
#include <metis.h>
#include <string>
#include "Cluster.h"
#include "CommunityLoader.h"

class MetisCL : public Cluster, public CommunityLoader  {
public:
    void LoadGraph(std::string filename);
    void Cluster();
    void LoadGroundTruth(std::map<int, int> &real_clusters, std::vector<int> &core_nodes);
    std::vector<int> CCluster(std::map<int, std::vector<int>>& grapht,
                                       std::map<std::string, std::string>& configt);
private:
    int current_truth_k;
    std::vector<idx_t> xadj_vec;
    std::vector<idx_t> adjncy_vec;

    int GetGroundK();
    void LoadGraphFromMap(std::map<int, std::vector<int>> &graphmap,
                      std::vector<idx_t> &xadj_v, std::vector<idx_t> &adjncy_v);
};

#endif // METIS_H

