#include "SpectralClustering.h"
#include "EigenSolver.h"
#include "KMeans.h"

#include <math.h>
#include <shogun/clustering/KMeans.h>
#include <shogun/distance/EuclideanDistance.h>
#include <iostream>

void SpectralClustering::Load(std::string filename){
    graph.Load(filename);
}

shogun::SGMatrix<float64_t> SpectralClustering::GetStandardMatrix(Graph &grapht){
    int nsize = grapht.GetSize();
    std::map<int, std::vector<int>> graphmap = grapht.GetGraph();
    shogun::SGMatrix<float64_t> graph_matrix(nsize, nsize);

    int i_sum, j_sum, node_i;
    for (auto& iter : graphmap){
        node_i = iter.first;
        i_sum = iter.second.size();

        for (auto& node_j : iter.second){
            j_sum = graphmap[node_j].size();
            graph_matrix(node_i-1, node_j-1) = static_cast<float64_t>(1.0/(sqrt(i_sum)*sqrt(j_sum)));
        }
    }

    return graph_matrix;
}


// stand-alone version of Spectral Clustering
void SpectralClustering::Cluster(){
    shogun::SGMatrix<float64_t> graph_matrix = GetStandardMatrix(graph);

    shogun::CMulticlassLabels* result = ClusterGraph(graph_matrix);
    for (int i=0; i<result->get_num_labels(); ++i)
        std::cout << i+1 << "\t" << result->get_label(i) << std::endl;

    // clean up
    SG_UNREF(result);
}


// ATTENTION SG_UNREF the return result object
// clustering on the given graph_matrix
shogun::CMulticlassLabels* SpectralClustering::ClusterGraph(shogun::SGMatrix<float64_t>& graph_matrix){
    EigenSolver solver;
    shogun::SGMatrix<float64_t> k_eigenvectors =
            solver.GetTopKEigenvectorsMatrix(
                graph_matrix, -1,
                std::min(100, (graph_matrix.num_rows-1)/4));

    int num_clusters = k_eigenvectors.num_rows;
    shogun::CDenseFeatures<float64_t>* features=new shogun::CDenseFeatures<float64_t>();
    features->set_feature_matrix(k_eigenvectors);
    SG_REF(features);


    /* create distance */
    shogun::CEuclideanDistance* distance=new shogun::CEuclideanDistance(features, features);

    /* create distance machine */
    shogun::CKMeans* clustering=new shogun::CKMeans(num_clusters, distance);
    clustering->train(features);

    /* build clusters */
    shogun::CMulticlassLabels* result=shogun::CLabelsFactory::to_multiclass(clustering->apply());

    // clean up
    SG_UNREF(clustering);
    SG_UNREF(features);
    return result;
}

//this prepares the matrix for the kCoreCluster version
shogun::SGMatrix<float64_t> SpectralClustering::GetCoreGraphMatrix(std::map<int, std::vector<int>>& coregraph){
    int nsize = coregraph.size();

    shogun::SGMatrix<float64_t> graph_matrix(nsize, nsize);

    int i_sum, j_sum, node_i;
    for (auto& iter : coregraph){
        node_i = iter.first;
        i_sum = iter.second.size();

        for (auto& node_j : iter.second){
            j_sum = coregraph[node_j].size();
            graph_matrix(node_i, node_j) = static_cast<float64_t>(1.0/(sqrt(i_sum)*sqrt(j_sum)));
        }
    }

    return graph_matrix;
}




// this is used for the CoreCluster
std::vector<int> SpectralClustering::CCluster(std::map<int, std::vector<int>>& grapht,
                                       std::map<std::string, std::string>& configt){
    std::vector<int> clusters;
    shogun::SGMatrix<float64_t> graph_matrix = GetCoreGraphMatrix(grapht);
    shogun::CMulticlassLabels* result = ClusterGraph(graph_matrix);
    for (int i=0; i<result->get_num_labels(); ++i)
        clusters.push_back(result->get_label(i));

    // clean up
    SG_UNREF(result);

    return clusters;
}
