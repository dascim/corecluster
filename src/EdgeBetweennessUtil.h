#ifndef COMMUNITYEDGEBETWEENNESSUTIL
#define COMMUNITYEDGEBETWEENNESSUTIL

#include <string>

//CEB stands for CommunityEdgeBetweenness
namespace CEB {
    void StandardClusterRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartEdgeBetweenness(int argc, char * argv[]);
}

#endif // COMMUNITYEDGEBETWEENNESSUTIL

