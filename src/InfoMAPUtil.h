#ifndef INFOMAPUTIL
#define INFOMAPUTIL

#include <string>

namespace IFM {
    void StandardClusterRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartInfoMAP(int argc, char * argv[]);
}

#endif // INFOMAPUTIL

