#include "InfoMAPUtil.h"

#include "CoreCluster.h"
#include "InfoMAP.h"

#include <iostream>
#include <ctime>
#include <cstring>

void IFM::StandardClusterRun(std::string graphfile){

    std::cerr << "#------------------------#" << std::endl;
    //start the timer
    std::clock_t begin = clock();

    InfoMAP *cl = new InfoMAP();

    cl->LoadFile(graphfile);
    cl->Cluster();

    delete cl;

    // stop the timer
    std::clock_t end = clock();

    double elapsed_time = static_cast<double>(end-begin) / CLOCKS_PER_SEC;
    std::cerr << "Elapsed time: " << elapsed_time << std::endl;
}

void IFM::CoreClusterRun(std::string graphfile){

    std::cerr << "#------------------------#" << std::endl;

    InfoMAP* cl = new InfoMAP();
    CoreCluster* cc = new CoreCluster(cl);

    //start the timer
    std::clock_t begin = clock();

    cc->Load(graphfile);

    //manually call core decomposition in order
    //to measure the time
    cc->CoreDecomposition();
    //cc->printCores();

    std::clock_t end = clock();
    double core_decomp_time = static_cast<double>(end-begin) / CLOCKS_PER_SEC;


    //start again the timer
    begin = clock();

    cc->CCluster();

    delete cl;
    delete cc;

    // stop the timer
    end = clock();

    double core_alg_time = static_cast<double>(end-begin) / CLOCKS_PER_SEC;
    double elapsed_time = core_decomp_time + core_alg_time;

    std::cerr << "Core decomposition time: " << core_decomp_time << std::endl
              << "Core algorithm time: " << core_alg_time << std::endl
            << "Elapsed time: " << elapsed_time << std::endl;

}

void IFM::Usage(char prog[]){
    std::cerr << "Usage: " << prog << "ceb [c|s] input_graph_file" << std::endl
     << " pass 'c' for corecluster or 's' for standard cluster" << std::endl
     << " example: " << prog << " ifm c graph.txt" << std::endl;
}


int IFM::StartInfoMAP(int argc, char * argv[]){
    // #TODO add also the config file argument, we will propably need it
    // for the other cluster algorithms (other than spectral)

    // simple argument parsing:
    if (argc != 4){
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    char* mode = argv[2];

    if (strcmp(mode, "c") !=0 && strcmp(mode, "s") != 0){
        Usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string graphfile = static_cast<std::string>(argv[3]);


    if (strcmp(mode, "s") == 0){
        StandardClusterRun(graphfile);
    }
    else if(strcmp(mode, "c") == 0){
        CoreClusterRun(graphfile);
    }

    return 0;
}



