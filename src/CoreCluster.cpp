#include "CoreCluster.h"
#include "Restruct.h"
#include <map>
#include <string>
#include <vector>
#include <set>

CoreCluster::CoreCluster(Cluster* clusterobj, bool ground_truth) {
    max_core = 0;
    max_cluster_id = 0;
    cluster_alg = clusterobj;
    use_ground_truth = ground_truth;
}

// this is in a different method so we can measure the
// execution time
void CoreCluster::CoreDecomposition() {
    cores = graph.Coreness();
}

// load the graph file
void CoreCluster::Load(std::string filename) {
    graph.Load(filename);
}

// things to be done before the cluster function
void CoreCluster::CoreClusterSetup() {
    // generally this should have already been called
    // if not then call it now
    if (cores.empty())
        CoreDecomposition();

    int current_core;

    // note here that the i (index) is
    // the node_id of the nodes in the graph
    for (int i = 1; i <= graph.GetSize(); i++) {
        current_core = cores[i];

        if (current_core > max_core)
            max_core = current_core;

        // also in the same loop fill the core_nodes map
        core_nodes[current_core].push_back(i);
    }
}

// load the k coregraph(to be passed for clustering) and returns
// a vector with the initial positions of the nodes
std::map<int, int> CoreCluster::LoadCoreGraph(int k) {
    std::vector<int>& k_nodes = core_nodes[k];
    std::map<int, std::vector<int>> graphmap = graph.GetGraph();
    std::map<int, int> positions;

    // store the positions of the current k core nodes
    for (size_t i = 0; i < k_nodes.size(); i++)  {
        positions[k_nodes[i]] = i;
    }

    if (!coregraph.empty()) {
        coregraph.clear();
    }

    for (auto& node_id : k_nodes) {
        std::vector<int> neighbors;

        // for each neighbor of the current node
        for (auto& nnode_id : graphmap[node_id]) {
            // ignore neighbors who are not in the current core
            if (cores[nnode_id] != k)
                continue;
            // store the neighbors witht the new positions indexes
            neighbors.push_back(positions[nnode_id]);
        }
        coregraph[positions[node_id]] = neighbors;
    }

    return positions;
}

std::map<int, std::vector<int>> CoreCluster::GetGraph(
        std::vector<int> &node_ids_v, std::map<int, int>& positions) {
    std::map<int, std::vector<int>> graphmap = graph.GetGraph();
    // graph to be clustered
    std::map<int, std::vector<int>> graphc;
    std::set<int> node_ids(node_ids_v.begin(), node_ids_v.end());

    for (auto& node_id : node_ids_v) {
        std::vector<int> neighbors;

        for (auto& nnode_id : graphmap[node_id]) {
            // if the neighbor is not among the given nodes continue
            if (node_ids.count(nnode_id) == 0)
                continue;

            neighbors.push_back(positions[nnode_id]);
        }
        graphc[positions[node_id]] = neighbors;
    }

    return graphc;
}

void CoreCluster::UpdateClusters(
        std::vector<int>& clusters, std::map<int, int>& positions) {
    // we suppose that cluster ids are integers.
    // each time we update the clusters and store the new ids
    // we start indexing from the previous max int id + 1
    // node_clusters.clear();
    int current_index = max_cluster_id + 1;
    int new_cluster_id, node_id;

    for (auto& iter : positions) {
        node_id = iter.first;
        // iter.second contain the position of the node_id
        new_cluster_id = clusters[iter.second] + current_index;

        if (new_cluster_id > max_cluster_id) {
            max_cluster_id = new_cluster_id;
        }

        // store the node_id with his new cluster id
        node_clusters[node_id] = new_cluster_id;
    }
}

void CoreCluster::printClusters() {
    for (auto& iter : node_clusters) {
        printf("%d %d\n", iter.first, iter.second);
    }
}

// #TODO think if possible to overcome O(2nlogn) with a better way here
std::map<int, std::vector<int> > CoreCluster::GetGraphWithCoreNeighbors(
    int current_core, std::map<int, std::vector<int> > &grapht) {
    std::vector<int>& temp_nodes = core_nodes[current_core];
    std::set<int> current_core_nodes(temp_nodes.begin(), temp_nodes.end());
    std::map<int, std::vector<int> > graph_core_neighbors;

    // loop over a map here
    for (auto& iter : node_clusters) {
        int node_id = iter.first;
        std::vector<int> neighbors_temp;

        for (auto& neighbor_id : grapht[node_id]) {
            if (current_core_nodes.count(neighbor_id) > 0 ||
                    node_clusters.count(neighbor_id) > 0) {
                neighbors_temp.push_back(neighbor_id);
            }
        }
        graph_core_neighbors[node_id] = neighbors_temp;
    }

    // loop over a vector here
    for (auto& node_id : current_core_nodes) {
        std::vector<int> neighbors_temp;

        for (auto& neighbor_id : grapht[node_id]) {
            if (current_core_nodes.count(neighbor_id) > 0 ||
                    node_clusters.count(neighbor_id) > 0) {
                neighbors_temp.push_back(neighbor_id);
            }
        }
        graph_core_neighbors[node_id] = neighbors_temp;
    }

    return graph_core_neighbors;
}

void CoreCluster::CCluster() {
    // things that have to be done before starting this function
    // e.g. core decomposition
    CoreClusterSetup();
    config["max_core"] = std::to_string(max_core);
    config["current_core"] = config["max_core"];

    // make a reference once here, it is used in several places
    std::map<int, std::vector<int>> graphmap_global = graph.GetGraph();

    // load the kcore-graph and save the position of the nodes
    std::map<int, int> core_positions = LoadCoreGraph(max_core);
    // cluster the k-core nodes, initial clustering
    if (use_ground_truth) {
        cluster_alg->LoadGroundTruth(real_clusters, core_nodes[max_core]);
    }
    std::vector<int> nclusters = cluster_alg->CCluster(coregraph, config);
    UpdateClusters(nclusters, core_positions);

    // now run the CoreCluster for each core
    for (int current_core = max_core-1; current_core >=1; current_core--) {
        // pass to the config the current core
        config["current_core"] = std::to_string(current_core);

        if (core_nodes.count(current_core) == 0)
            continue;

        std::map<int, std::vector<int>> graphmap = GetGraphWithCoreNeighbors(
                                                       current_core,
                                                       graphmap_global);

        Restruct* restruct = new Restruct();
        // NOTE: node_clusters get updated with new nodes and
        // core_nodes[current_core] change currently the
        // core_nodes[current_core] contains the unclustered
        // nodes of core k-1
        restruct->Select(node_clusters, max_cluster_id,
                      core_nodes[current_core], graphmap);

        if (restruct->HasRemainingNodes()) {
            std::vector<int>& remaining_nodes =
                    *(restruct->GetRemainingNodes());
            std::map<int, int>& remaining_nodes_pos =
                    *(restruct->GetRemainingPositions());

            // cluster again the remaining connected together nodes
            std::map<int, std::vector<int>> graphc = GetGraph(remaining_nodes,
                                                         remaining_nodes_pos);
            if (use_ground_truth) {
                cluster_alg->LoadGroundTruth(real_clusters, remaining_nodes);
            }
            nclusters = cluster_alg->CCluster(graphc, config);
            UpdateClusters(nclusters, remaining_nodes_pos);
        }

        // handle the rest unclustered nodes
        restruct->Join(node_clusters, max_cluster_id, graphmap);

        /* clean up */
        delete restruct;
    }

    // return node_clusters;
    printClusters();
}

void CoreCluster::printCores() {
    for (auto& node : cores) {
        printf("%d\n", node);
    }
}
