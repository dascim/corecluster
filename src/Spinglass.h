#ifndef SPINGLASS
#define SPINGLASS

#include "Cluster.h"
#include "Graph.h"
#include "IGraphShared.h"
#include "CommunityLoader.h"

class Spinglass : public Cluster, public IGraphShared, public CommunityLoader  {
public:
    void LoadFile(std::string filename);
    void Cluster();
    std::vector<int> CCluster(std::map<int, std::vector<int>>& grapht,
                                       std::map<std::string, std::string>& configt);
    void LoadGroundTruth(std::map<int, int> &real_clusters, std::vector<int> &core_nodes);
private:
    Graph graph;
    int current_truth_k;
    int GetGroundK();
};

#endif // SPINGLASS

