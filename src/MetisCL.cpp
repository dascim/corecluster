#include "MetisCL.h"
#include <fstream>
#include <sstream>
#include <set>

/*
 *load the input file in CSR sparse format as mentioned
 *in METIS documentation
 */
void MetisCL::LoadGraph(std::string filename){
    //first value
    xadj_vec.push_back(0);

    std::ifstream graphfile(filename);

    std::string line;
    while(std::getline(graphfile, line)){
        std::string token;
        std::istringstream sline( line );

        // read the first token which is the node id
        std::getline(sline, token, '\t');

        // loop over the rest of the tokens/neighbors of the node
        while (std::getline(sline, token, '\t')){
            adjncy_vec.push_back( std::stoi(token)-1 );
        }
        xadj_vec.push_back( adjncy_vec.size() );
    }
    graphfile.close();
}

// LoadCommunity must be called first
int MetisCL::GetGroundK(){
    if (real_clusters.empty())
        return -1;

    std::set<int> uniq_cls;

    for (auto& iter : real_clusters){
        uniq_cls.insert(iter.second);
    }

    return uniq_cls.size();
}

void MetisCL::LoadGroundTruth(
        std::map<int, int> &real_clusters, std::vector<int> &core_nodes){

    std::set<int> uniq_cls;

    for (auto& node_id : core_nodes){
        uniq_cls.insert(real_clusters[node_id]);
    }

    current_truth_k = uniq_cls.size();
}

void MetisCL::LoadGraphFromMap(std::map<int, std::vector<int> > &graphmap,
                               std::vector<idx_t> &xadj_v, std::vector<idx_t> &adjncy_v){
    //first value
    xadj_v.push_back(0);

    for (auto& iter : graphmap){
        // loop over the rest of the tokens/neighbors of the node
        for (auto& node_id : iter.second){
            adjncy_v.push_back( node_id );
        }
        xadj_v.push_back( adjncy_v.size() );
    }
}

std::vector<int> MetisCL::CCluster(std::map<int, std::vector<int>>& grapht,
                                   std::map<std::string, std::string>& configt){

    std::vector<int> core_clusters(grapht.size());

    //in this case no need to cluster
    if (current_truth_k <= 1){
        //for (size_t i = 0; i < grapht.size(); i++){
        //    core_clusters[i] = 1;
        //}
        return core_clusters;
    }

    std::vector<idx_t> xadj_v;
    std::vector<idx_t> adjncy_v;

    LoadGraphFromMap(grapht, xadj_v, adjncy_v);

    idx_t options[METIS_NOPTIONS];
    METIS_SetDefaultOptions(options);
    options[METIS_OPTION_IPTYPE] = METIS_PTYPE_KWAY;
    options[METIS_OPTION_NUMBERING] = 0;
    options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
    options[METIS_OPTION_CTYPE]   = METIS_CTYPE_SHEM;
    options[METIS_OPTION_NO2HOP]  = 0;
    options[METIS_OPTION_MINCONN] = 0;
    options[METIS_OPTION_CONTIG]  = 0;
    options[METIS_OPTION_SEED]    = -1;
    options[METIS_OPTION_NITER]   = 10; //default anyway
    options[METIS_OPTION_NCUTS]   = 1;
    //options[METIS_OPTION_DBGLVL]  = 0;//METIS_DBG_INFO;

    idx_t nvtxs = grapht.size();
    idx_t ncon = 1; // #of constraints
    idx_t* xadj = xadj_v.data();
    idx_t* adjncy = adjncy_v.data();
    idx_t nparts = current_truth_k;
    idx_t objval;
    idx_t* part = new idx_t[nvtxs];

    //parameters given as described in METIS documentation
    //this is the minimal configuration
    METIS_PartGraphKway(&nvtxs, &ncon, xadj, adjncy, NULL, NULL, NULL,
                        &nparts, NULL, NULL, options, &objval, part);

    //now the clusters to vector
    for (size_t i = 0; i < grapht.size(); i++){
        core_clusters[i] = part[i];
    }

    delete[] part;

    return core_clusters;
}


void MetisCL::Cluster(){
    //ground truth k partitions
    int k = GetGroundK();

    idx_t options[METIS_NOPTIONS];
    METIS_SetDefaultOptions(options);
    options[METIS_OPTION_IPTYPE] = METIS_PTYPE_KWAY;
    options[METIS_OPTION_NUMBERING] = 0;
    options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
    options[METIS_OPTION_CTYPE]   = METIS_CTYPE_SHEM;
    options[METIS_OPTION_NO2HOP]  = 0;
    options[METIS_OPTION_MINCONN] = 0;
    options[METIS_OPTION_CONTIG]  = 0;
    options[METIS_OPTION_SEED]    = -1;
    options[METIS_OPTION_NITER]   = 10; //default anyway
    options[METIS_OPTION_NCUTS]   = 1;
    //options[METIS_OPTION_DBGLVL]  = 0;//METIS_DBG_INFO;

    idx_t nvtxs = real_clusters.size();
    idx_t ncon = 1; // #of constraints
    idx_t* xadj = xadj_vec.data();
    idx_t* adjncy = adjncy_vec.data();
    idx_t nparts = k;
    idx_t objval;
    idx_t* part = new idx_t[nvtxs];

    //parameters given as described in METIS documentation
    //this is the minimal configuration
    METIS_PartGraphKway(&nvtxs, &ncon, xadj, adjncy, NULL, NULL, NULL,
                        &nparts, NULL, NULL, options, &objval, part);
    //now dump the clusters
    for (size_t i = 0; i < real_clusters.size(); i++){
        printf("%lu\t%d\n", i+1, part[i]);
    }

    delete[] part;
}
