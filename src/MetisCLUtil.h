#ifndef METISCLUTIL
#define METISCLUTIL

#include <string>

namespace ME {
    void StandardMetisRun(
            std::string graphfile, std::string communityfile);
    void CoreMetisRun(
            std::string graphfile, std::string communityfile);
    void Usage(char prog[]);
    int StartMetisCL(int argc, char * argv[]);
}

#endif // METISCLUTIL

