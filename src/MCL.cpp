#include "MCL.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include "GlobalTimer.h"

void MCL::LoadFile(std::string filename){
    graph.Load(filename);
}

void MCL::DumpMCLFormat(std::string output_file, std::map<int, std::vector<int>>& graphmap, bool core_mode){

    std::ofstream fout(output_file, std::ofstream::out);

    fout << "(mclheader\n"  << "mcltype matrix\n" <<
            "dimensions " << graphmap.size() <<
            "x" << graphmap.size() <<
            "\n)\n" << "(mclmatrix\n" <<
            "begin\n";
    int write_node;
    for (auto& iter : graphmap){
        if(core_mode)
            write_node = iter.first;
        else
            write_node = iter.first-1;

        fout << write_node << " ";

        for (auto& node_id : iter.second){
            if (core_mode)
                write_node = node_id;
            else
                write_node = node_id-1;

            fout << write_node << " ";
        }
        fout << "$\n";
    }
    fout << ")\n";
    fout.close();
}

std::vector<int> MCL::LoadMCLOutput(std::string filename){
    std::ifstream cluster_file(filename);
    std::vector<int> clusters;
    bool is_data = false;
    bool cluster_start = true;
    std::string line;
    std::string token;
    int dimensions = -1;
    int current_cluster = -1;
    int index = -1;

    while(std::getline(cluster_file, line)){
        if (is_data){
            std::istringstream sline( line );
            if (cluster_start){
                std::getline(sline, token, ' ');

                //the end of file
                if (token.find(")") == 0)
                    break;

                current_cluster = std::stoi(token) + 1;
                cluster_start = false;
            }

            while (std::getline(sline, token, ' ')){
                if (token.empty()){
                    continue;
                }
                else if (token.find('$') == 0){
                    cluster_start = true;
                }
                else{
                    index = std::stoi(token);
                    clusters[index] = current_cluster;
                }
            }
        }
        else if (line.find("begin") == 0){
            is_data = true;
            continue;
        }
        else if (line.find("dimensions") == 0){
            std::istringstream sline( line );

            //throw the first token dimensions
            std::getline(sline, token, ' ');
            //keep the dimensions
            std::getline(sline, token, ' ');

            std::istringstream sdim( token );
            std::getline(sdim, token, 'x');
            dimensions = std::stoi(token);
            clusters.resize(dimensions);
        }
    }
    cluster_file.close();

    return clusters;
}

std::string MCL::SubProcessExec(const std::string cmd) {
    FILE* pipe = popen(cmd.c_str(), "r");
    if (!pipe) return "ERROR";
    char buffer[512];
    std::stringstream result;
    while(!feof(pipe)) {
        if(fgets(buffer, 512, pipe) != NULL)
            result << buffer;
    }
    pclose(pipe);
    return result.str();
}

std::string MCL::GetMCLCommand() {
    std::stringstream ss;
    // -q x = make mcl quiet
    ss << "export TIMEFORMAT=\"%5R\";(time mcl "
       << dump_graph_file
       << " -o "
       << dump_clusters_file
       << " -q x) 2> cmd.tmp;cat cmd.tmp;rm cmd.tmp";

    return ss.str();
}

std::string MCL::GetFixedToken(std::string raw_output) {
    std::stringstream ss (raw_output);
    std::string token;
    //throw first line with [mcl]
    std::getline(ss, token);
    std::getline(ss, token);

    return token;
}

std::vector<int> MCL::Cluster() {
    std::map<int, std::vector<int>> graphmap = graph.GetGraph();
    GT::timer.Stop();
    DumpMCLFormat(dump_graph_file, graphmap);
    GT::timer.Start();

    std::string cmd = GetMCLCommand();
    std::string extra_time_str = SubProcessExec(cmd.c_str());
    if (extra_time_str.compare("ERROR") == 0) {
        std::cerr << "Potato happend in MCL subprocess" << std::endl;
        exit(EXIT_FAILURE);
    }
    else {
        if (extra_time_str.find("[mcl]") != std::string::npos){
            extra_time_str = GetFixedToken(extra_time_str);
        }
        double extra_time = stod(extra_time_str);
        GT::timer.AddToClock(extra_time);
    }

    GT::timer.Stop();
    std::vector<int> clusters = LoadMCLOutput(dump_clusters_file);
    GT::timer.Start();

    return clusters;
}

std::vector<int> MCL::CCluster(std::map<int, std::vector<int>>& grapht,
                               std::map<std::string, std::string>& configt) {
    // #TODO ATTENTION check in the core cluster
    // case do not use -1 in the file dump
    GT::timer.Stop();
    DumpMCLFormat(dump_graph_file, grapht, true);
    GT::timer.Start();

    std::string cmd = GetMCLCommand();
    std::string extra_time_str = SubProcessExec(cmd.c_str());
    if (extra_time_str.compare("ERROR") == 0) {
        std::cerr << "Potato happend in MCL subprocess" << std::endl;
        exit(EXIT_FAILURE);
    }
    else {
        if (extra_time_str.find("[mcl]") != std::string::npos){
            extra_time_str = GetFixedToken(extra_time_str);
        }
        double extra_time = stod(extra_time_str);
        GT::timer.AddToClock(extra_time);
    }

    GT::timer.Stop();
    std::vector<int> cls = LoadMCLOutput(dump_clusters_file);
    GT::timer.Start();

    return cls;
}
