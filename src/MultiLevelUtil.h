#ifndef MULTILEVELUTIL
#define MULTILEVELUTIL

#include <string>

namespace ML {
    void StandardClusterRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartMultiLevel(int argc, char * argv[]);
}
#endif // MULTILEVELUTIL

