#ifndef RESTRUCT_H
#define RESTRUCT_H

#include <vector>
#include <set>
#include <map>

class Restruct {
public:
    // writes new clustered nodes in node_clusters map
    void Select(std::map<int, int>& node_clusters, int max_cluster,
                std::vector<int>& unclustered_nodes, std::map<int, std::vector<int>>& grapht);

    void Join(std::map<int, int>& node_clusters,
              int max_cluster, std::map<int, std::vector<int>>& grapht);

    // returns the together_connected
    std::vector<int>* GetRemainingNodes();
    int GetRemainingNodesNum();
    //returns the together_connected_pos
    std::map<int, int>* GetRemainingPositions();
    bool HasRemainingNodes();

private:
    std::vector<int> neighbor_clusters; // node_ids which have at least one neighbor in a cluster
    std::vector<int> together_connected; // node_ids which do not have neighbors in any cluster
                                      // and they connected with each other
    std::map<int, int> together_connected_pos; // store the positions of the nodes
    std::vector<int> not_together_connected; // node_ids with no neighbors in a cluster and not
                                             // connected with each other

    void Optimize(std::map<int, int>& node_clusters, int max_cluster,
                  std::vector<int>& unclustered_nodes, std::map<int, std::vector<int>>& grapht);

    void SpecialHandleUnclusteredNodes(std::map<int, int>& node_clusters, int max_cluster,
                                       std::map<int, std::vector<int>>& grapht, std::vector<int>& bin);

    void Assign(std::map<int, int>& node_clusters, int max_cluster,
                std::map<int, std::vector<int>>& grapht);

    void HandleNeighborCluster(std::map<int, int>& node_clusters,
                               int max_cluster, std::map<int, std::vector<int>>& grapht);

    void HandleNotTogetherConnected(std::map<int, int>& node_clusters,
                                    int max_cluster, std::map<int, std::vector<int>>& grapht, std::vector<int>& bin);

    //#TODO check again special case handle, find if we really need this
    std::vector<int> GetBin(int max_cluster, std::map<int, int>& node_clusters);

    // #TODO move it to a proper h,cpp file if it used in another class
    void RemoveFromVector(std::vector<int>& vec, int index);
};

#endif // RESTRUCT_H
