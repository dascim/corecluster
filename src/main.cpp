#include <iostream>
#include "SpectralClusteringUtil.h"
#include "MCLUtil.h"
#include "MetisCLUtil.h"
#include "EdgeBetweennessUtil.h"
#include "FastGreedyUtil.h"
#include "MultiLevelUtil.h"
#include "InfoMAPUtil.h"
#include "LabelPropagationUtil.h"
#include "SpinglassUtil.h"
#include "LeadingEigenvectorUtil.h"
#include "WalkTrapUtil.h"

void Usage(){
    std::cerr << "You must provide a valid algorithm as the first argument:"
              << std::endl
              << "available algorithms:" << std::endl
              << " mcl -> MCL" << std::endl
              << " spc -> SpectralClustering" << std::endl
              << " metis -> Metis" << std::endl
              << " ceb -> EdgeBetweenness" << std::endl
              << " fg -> FastGreedy" << std::endl
              << " ml -> MultiLevel" << std::endl
              << " ifm -> InfoMAP" << std::endl
              << " lp -> LabelPropagation" << std::endl
              << " sg -> Spinglass" << std::endl
              << " leig -> LeadingEigenvector" << std::endl
              << " wt -> WalkTrap" << std::endl;
}

int main(int argc, char * argv[])
{
    if(argc == 1){
        Usage();
        exit(EXIT_FAILURE);
    }

    std::string algorithm = static_cast<std::string>(argv[1]);

    if (algorithm.compare("spc") == 0)
        return SC::StartSpectralClustering(argc, argv);
    else if (algorithm.compare("mcl") == 0)
        return MC::StartMCL(argc, argv);
    else if (algorithm.compare("metis") == 0)
        return ME::StartMetisCL(argc, argv);
    else if (algorithm.compare("ceb") == 0)
        return CEB::StartEdgeBetweenness(argc, argv);
    else if (algorithm.compare("fg") == 0)
        return FG::StartFastGreedy(argc, argv);
    else if (algorithm.compare("ml") == 0)
        return ML::StartMultiLevel(argc, argv);
    else if (algorithm.compare("ifm") == 0)
        return IFM::StartInfoMAP(argc, argv);
    else if (algorithm.compare("lp") == 0)
        return LP::StartLabelPropagation(argc, argv);
    else if (algorithm.compare("sg") == 0)
        return SG::StartSpinglass(argc, argv);
    else if (algorithm.compare("leig") == 0)
        return LEIG::StartLeadingEigenvector(argc, argv);
    else if (algorithm.compare("wt") == 0)
        return WT::StartWalkTrap(argc, argv);
    else {
        Usage();
        exit(EXIT_FAILURE);
    }
}


