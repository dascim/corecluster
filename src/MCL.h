#ifndef MCL_H
#define MCL_H

#include "Cluster.h"
#include "Graph.h"

class MCL : public Cluster {
public:
    MCL() { dump_clusters_file = "mcl_cls.temp"; dump_graph_file = "mcl_graph.tmp"; }
    void LoadFile(std::string filename);
    std::vector<int> Cluster();
    std::vector<int> CCluster(std::map<int, std::vector<int>>& grapht,
                                       std::map<std::string, std::string>& configt);
private:
    Graph graph;
    std::string dump_graph_file;
    std::string dump_clusters_file;
    void DumpMCLFormat(std::string output_file,
                       std::map<int, std::vector<int>>& graphmap, bool core_mode = false);
    std::vector<int> LoadMCLOutput(std::string filename);
    std::string SubProcessExec(const std::string cmd);
    std::string GetMCLCommand();
    std::string GetFixedToken(std::string raw_output);
};

#endif // MCL_H
