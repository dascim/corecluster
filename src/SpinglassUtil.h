#ifndef SPINGLASSUTIL
#define SPINGLASSUTIL

#include <string>

namespace SG {
    void StandardClusterRun(
            std::string graphfile, std::string communityfile);
    void CoreClusterRun(
            std::string graphfile, std::string communityfile);
    void Usage(char prog[]);
    int StartSpinglass(int argc, char * argv[]);
}

#endif // SPINGLASSUTIL

