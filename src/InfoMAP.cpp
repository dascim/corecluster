#include "InfoMAP.h"
#include <iostream>
#include <tuple>
#include <set>
#include <limits>

void InfoMAP::LoadFile(std::string filename){
    graph.Load(filename);
}

std::vector<int> InfoMAP::CCluster(std::map<int, std::vector<int> > &grapht,
                                        std::map<std::string, std::string> &configt){
    igraph_t g;
    InitializeIGraphObj(g, grapht, true);

    igraph_real_t codelength;
    int nb_trials = 1;
    igraph_vector_t membership;
    long int no_of_nodes = igraph_vcount(&g);

    igraph_vector_init(&membership, 0);

    //call the actual clustering function
    igraph_community_infomap(&g,
                 /*e_weights */ 0,
                 /*v_weights */ 0,
                 nb_trials,
                 &membership,
                 &codelength);

    std::vector<int> clusters(no_of_nodes);
    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        clusters[i] = VECTOR(membership)[i];
    }

    igraph_vector_destroy(&membership);
    igraph_destroy(&g);

    return clusters;
}

void InfoMAP::Cluster(){
    igraph_t g;
    std::map<int, std::vector<int>> graphmap = graph.GetGraph();
    InitializeIGraphObj(g, graphmap, false);

    //TODO ATTENTION this might affect a lot the performance,
    //it might be better to use a lower value for core cluster mode
    //attends to partition the graph
    igraph_real_t codelength;
    int nb_trials = 1;
    igraph_vector_t membership;
    long int no_of_nodes = igraph_vcount(&g);

    igraph_vector_init(&membership, 0);

    //call the actual clustering function
    igraph_community_infomap(&g,
                 /*e_weights */ 0,
                 /*v_weights */ 0,
                 nb_trials,
                 &membership,
                 &codelength);

    //print the clusters
    for (long int i = 0; i < no_of_nodes; i++){
        std::cout << i+1 << " " << VECTOR(membership)[i] << std::endl;
    }

    igraph_vector_destroy(&membership);
    igraph_destroy(&g);
}

