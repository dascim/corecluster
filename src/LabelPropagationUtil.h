#ifndef LABELPROPAGATIONUTIL
#define LABELPROPAGATIONUTIL

#include <string>

namespace LP {
    void StandardClusterRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartLabelPropagation(int argc, char * argv[]);
}

#endif // LABELPROPAGATIONUTIL

