#ifndef REALTIMER_H
#define REALTIMER_H

#include <ctime>
#include <chrono>

class RealTimer {
public:
    RealTimer(){ real_elapsed_time = 0.0; active = false; }
    void Start() {
        if (!active){
            start_time = clock();
            active = true;
        }
    }
    void Stop() {
        if (active){
            std::clock_t end = clock();
            real_elapsed_time +=  static_cast<double>(end-start_time) / CLOCKS_PER_SEC;
            active = false;
        }
    }
    void AddToClock(double extra_time) {
        real_elapsed_time += extra_time;
    }
    void Reset(){ real_elapsed_time = 0.0; active = false;}
    double GetElapsedTime() { return real_elapsed_time; }


private:
    std::clock_t start_time;
    double real_elapsed_time;
    bool active;
};

#endif // REALTIMER_H
