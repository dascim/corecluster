#ifndef SPECTRALCLUSTER_H
#define SPECTRALCLUSTER_H

#include <shogun/lib/SGMatrix.h>
#include <shogun/clustering/KMeans.h>

#include "Graph.h"
#include "Cluster.h"

class SpectralClustering : public Cluster {
public:
    // load the graph file
    void Load(std::string filename);
    // for the moment it's void
    void Cluster();
    std::vector<int> CCluster(std::map<int, std::vector<int>>& grapht,
                                       std::map<std::string, std::string>& configt);
    shogun::CMulticlassLabels* ClusterGraph(shogun::SGMatrix<float64_t>& graph_matrix);
private:
    Graph graph;
    // this prepares the matrix for the stand-alone version
    shogun::SGMatrix<float64_t> GetStandardMatrix(Graph& grapht);
    // this prepares the matrix for the kCoreCluster version
    shogun::SGMatrix<float64_t> GetCoreGraphMatrix(std::map<int, std::vector<int>>& coregraph);
};

#endif // SPECTRALCLUSTER_H
