#ifndef CORECLUSTER_H
#define CORECLUSTER_H

#include "Graph.h"
#include "Cluster.h"
#include "ConfigLoader.h"
#include "CommunityLoader.h"

#include <set>

class CoreCluster : public CommunityLoader, public ConfigLoader {
 public:
    // ATTENTION in case you enable ground truth, LoadCommunity
    // must be called before calling CCluster
    CoreCluster(Cluster* clusterobj, bool ground_truth = false);

    void Load(std::string filename);
    void CoreDecomposition();
    void CCluster();
    void printCores();

 private:
    Cluster* cluster_alg;
    int max_core;
    int max_cluster_id;
    bool use_ground_truth;
    Graph graph;
    // this will be overriden in each k loop
    std::map<int, std::vector<int>> coregraph;
    std::vector<int> cores;
    std::map<int, std::vector<int>> core_nodes;
    std::map<int, int> node_clusters;

    // load the coregraph map and return the positions of the selected nodes
    std::map<int, int> LoadCoreGraph(int k);
    std::map<int, std::vector<int>> GetGraph(
            std::vector<int>& node_ids_v, std::map<int, int>& positions);
    std::map<int, std::vector<int> > GetGraphWithCoreNeighbors(
            int current_core, std::map<int, std::vector<int> > &grapht);
    void UpdateClusters(std::vector<int>& clusters,
                        std::map<int, int>& positions);
    void CoreClusterSetup();

    // #TODO remove them, for debug
    void printClusters();
};

#endif // CORECLUSTER_H
