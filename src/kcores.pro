TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11
CONFIG -= qt

# this in order to have c++11 in qmake-qt4
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    EigenSolver.cpp \
    ConfigLoader.cpp \
    CoreCluster.cpp \
    Restruct.cpp \
    Graph.cpp \
    KMeans.cpp \
    SpectralClusteringUtil.cpp \
    SpectralClustering.cpp \
    MCL.cpp \
    MCLUtil.cpp \
    CommunityLoader.cpp \
    MetisCL.cpp \
    MetisCLUtil.cpp \
    GlobalTimer.cpp \
    IGraphShared.cpp \
    EdgeBetweenness.cpp \
    EdgeBetweennessUtil.cpp \
    FastGreedy.cpp \
    FastGreedyUtil.cpp \
    MultiLevel.cpp \
    MultiLevelUtil.cpp \
    InfoMAP.cpp \
    InfoMAPUtil.cpp \
    LabelPropagation.cpp \
    LabelPropagationUtil.cpp \
    Spinglass.cpp \
    SpinglassUtil.cpp \
    LeadingEigenvector.cpp \
    LeadingEigenvectorUtil.cpp \
    WalkTrap.cpp \
    WalkTrapUtil.cpp
    #ncp.cpp \
    #LocalSpectralClustering.cpp \
    #LocalSpectralClusteringUtil.cpp

HEADERS += \
    Cluster.h \
    EigenSolver.h \
    ConfigLoader.h \
    CoreCluster.h \
    Restruct.h \
    KMeans.h \
    Graph.h \
    SpectralClusteringUtil.h \
    SpectralClustering.h \
    MCL.h \
    MCLUtil.h \
    maxheap.h \
    vektor.h \
    fastcommunity_mh.h \
    FastCommunityUtil.h \
    CommunityLoader.h \
    MetisCL.h \
    MetisCLUtil.h \
    RealTimer.h \
    GlobalTimer.h \
    IGraphShared.h \
    EdgeBetweenness.h \
    EdgeBetweennessUtil.h \
    FastGreedy.h \
    FastGreedyUtil.h \
    MultiLevel.h \
    MultiLevelUtil.h \
    InfoMAP.h \
    InfoMAPUtil.h \
    LabelPropagation.h \
    LabelPropagationUtil.h \
    Spinglass.h \
    SpinglassUtil.h \
    LeadingEigenvector.h \
    LeadingEigenvectorUtil.h \
    WalkTrap.h \
    WalkTrapUtil.h
    #ncp.h \
    #LocalSpectralClustering.h \
    #LocalSpectralClusteringUtil.h

# snap library
#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../snap/snap-core/release/ -lsnap
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../snap/snap-core/debug/ -lsnap
#else:unix: LIBS += -L$$PWD/../snap/snap-core/ -lsnap

#INCLUDEPATH += $$PWD/../snap/snap-core
#INCLUDEPATH += $$PWD/../snap/glib-core


# shogun library
win32:CONFIG(release, debug|release): LIBS += -L/usr/local/lib/release/ -lshogun
else:win32:CONFIG(debug, debug|release): LIBS += -L/usr/local/lib/debug/ -lshogun
else:unix: LIBS += -L/usr/local/lib/ -lshogun


win32:CONFIG(release, debug|release): LIBS += -L/usr/local/lib/release/ -lmetis
else:win32:CONFIG(debug, debug|release): LIBS += -L/usr/local/lib/debug/ -lmetis
else:unix: LIBS += -L$$/usr/local/lib/ -lmetis

INCLUDEPATH += /usr/local/include
DEPENDPATH += /usr/local/include

win32:CONFIG(release, debug|release): LIBS += -L/usr/lib/release/ -ligraph
else:win32:CONFIG(debug, debug|release): LIBS += -L/usr/lib/debug/ -ligraph
else:unix: LIBS += -L/usr/lib/ -ligraph

INCLUDEPATH += /usr/include
DEPENDPATH += /usr/include
