#ifndef FASTCOMMUNITYUTIL
#define FASTCOMMUNITYUTIL

#include <string.h>
#include <string>

namespace FC {
    void StandardFCRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartFC(int argc, char * argv[]);
}

#endif // FASTCOMMUNITYUTIL

