#ifndef SPECTRALCLUSTERINGUTIL_H
#define SPECTRALCLUSTERINGUTIL_H

#include <string>

namespace SC {
    void StandardClusterRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartSpectralClustering(int argc, char * argv[]);
}
#endif // SPECTRALCLUSTERINGUTIL_H
