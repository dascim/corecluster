#ifndef LEADINGEIGENVECTORUTIL
#define LEADINGEIGENVECTORUTIL

#include <string>

namespace LEIG {
    void StandardClusterRun(std::string graphfile);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartLeadingEigenvector(int argc, char * argv[]);
}

#endif // LEADINGEIGENVECTORUTIL

