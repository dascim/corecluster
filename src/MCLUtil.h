#ifndef MCLUTIL_H
#define MCLUTIL_H

#include <string.h>
#include <string>

namespace MC {
    void StandardMCLRun(std::string graphfile, std::string fileout);
    void CoreClusterRun(std::string graphfile);
    void Usage(char prog[]);
    int StartMCL(int argc, char * argv[]);
}

#endif // MCLUTIL_H
