The following instructions show you how to compile the project 
under a clean(fresh) installation of Linux Mint 17.2 step by step.


# DEPENDENCIES

Core dependencies

## C++ compiler

Make sure you have a c++ compiler, if not install g++ via:

```
$ sudo apt-get install g++
```

## qmake (version 4 or 5)

The project uses qmake, install it with:

```
$ sudo apt-get install qt4-qmake
```

## git (optional)

You will need git in order to download the project's source code, alternatively you
can download a zip file with the source code from bitbucket in case you don't want to 
mess with git.

In order to install it:

```
$ sudo apt-get install git
```

# Libraries

## [Shogun](http://www.shogun-toolbox.org/)

You can install shogun from the official repo via the package libshogon-dev:

```
$ sudo apt-get install libshogun-dev
```

In case apt does not find the package you can search for it via:

```
$ apt-cache search shogun
```

## [Metis](http://glaros.dtc.umn.edu/gkhome/views/metis)

Download metis tar from here: http://glaros.dtc.umn.edu/gkhome/metis/metis/download

```
$ wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz
$ tar xvf metis-5.1.0.tar.gz
$ cd metis-5.1.0/
```

In order to compile metis you will need to have cmake installed:

```
$ sudo apt-get install cmake
```

And then

```
$ # run make in order to see instructions how to compile metis
$ make config
$ make
$ sudo make install
```

## [MCL](http://www.micans.org/mcl/index.html)

Download MCL from: http://www.micans.org/mcl/index.html?sec_software

```
$ wget http://www.micans.org/mcl/src/mcl-14-137.tar.gz
$ tar xvf mcl-14-137.tar.gz
$ cd mcl-14-137/
```

Inside mcl directory read the INSTALL file for instructions how to compile and install mcl.  
The steps should be the following:

```
$ # here we assume that we are inside the mcl-14-137 directory
$ ./configure
$ make
$ sudo make install
```

## [igraph](http://igraph.org)

First of all make sure you install libz-dev and libxml2-dev:

```
$ sudo apt-get install libz-dev libxml2-dev
```

Download the source code from here: http://igraph.org/c/#downloads and then compile and install 
the package:

```
$ wget http://igraph.org/nightly/get/c/igraph-0.7.1.tar.gz
$ tar xvf igraph-0.7.1.tar.gz
$ cd igraph-0.7.1/
$ make
$ sudo make install #(or installcheck to also run the tests)
```

## Dependencies for metrics RI/NMI/Conductance and Plots

In case you want to use the python script to produce metrics RI/NMI/Conductance 
for the clustering and the scripts which make the plots you will need the following:

* python (>= 2.7) and python-dev
* numpy
* cython
* matplotlib

### [Python](https://www.python.org/)

Your linux should already have python installed, just make sure you have the required version:

```
$ python
$ # to exit the python interpreter: quit() or Ctrl-D
```

Afterwards manually install python-dev:

```
$ sudo apt-get install python-dev
```

### [Numpy](http://www.numpy.org/) and [Cython](http://cython.org/)

You can install these 2 using pip. First install pip:

```
$ sudo apt-get install python-pip
```

### [Matplotlib](http://matplotlib.org/)

First make sure you install libfreetype and libpng

```
$ sudo apt-get install libfreetype6-dev libpng12-dev
```

Afterwards

```
$ sudo pip install matplotlib
```

And afterwards install the packages through pip:

```
$ sudo pip install numpy cython
```

Having all the above you will be able to build the project.


HOW TO COMPILE CORE CLUSTER
===============

Make sure you have installed all the dependencies.

Let's assume that you will use git to get the code (or you can download a zip with 
the source code from bitbucket)

```
$ git clone https://bitbucket.org/alamages/core-cluster
$ cd core-cluster/
```

Once you are inside the project's folder compile the code:

```
$ cd build/
$ qmake ../src/
$ make
```

If you installed all the dependencies correctly with the above you should succesfully compile the code.
In case make is missing some package you can take a look into the src/kcores.pro file and make sure that 
the libraries' system paths agree with the paths of your installed libraries, if not then modify the src/kcores.pro with the proper paths and re-build the project.

## Compile the metrics

```
$ cd metrics/
$ make
```

* Note1: You must have installed python, python-dev, numpy and cython (check dependencies section how to do that).
* Note2: In case you explicitly use python3 (or it's your system's default) edit the metrics/Makefile and replace python2 with python3.

# HOW TO USE

## Run the code
Once you have successfully compiled the project and you are inside the build directory:

```
$ # in order to run kcores
$ ./kcores <algorithm> <mode> <graph_file>
```

The **algorithm** argument can be one of the following abbreviations:

 * mcl -> MCL
 * spc -> SpectralClustering
 * metis -> Metis
 * ceb -> EdgeBetweenness
 * fg -> FastGreedy
 * ml -> MultiLevel
 * ifm -> InfoMAP
 * lp -> LabelPropagation
 * sg -> Spinglass
 * leig -> LeadingEigenvector
 * wt -> WalkTrap


 The **mode** argument can be:

 * 's' for standard version of the algorithm
 * 'c' for core clustering mode/version


And the **graph_file** argument is a txt file which contains the graph in the following format:

```
node_id1 node_id1_neighbor1 node_id1_neighbor2 ...
node_id2 node_id2_neighbor_1 node_id2_neighbor2 ...
```
You can find an example graph file in the data/graph.txt.


In order to run **standard** *Spectal Clustering*:

```
$ ./kcores spc s ../data/graph.txt
```

In order to run **core** *Spectral Clustering*:

```
$ ./kcores spc c ../data/graph.txt
```

**NOTE**: The algorithms **Metis** and **Spinglass** apart from the graph file
they also need a ground truth community file (which contains the community id of each node in the graph) in order to run successfully. So in order to run ,for example, standard metis you have to:

```
$ # ./kcores <algorithm> <mode> <graph_file> <community_file>
$ ./kcores metis s ../data/graph.txt ../data/community.dat
```

Another example, run Spinglass in core cluster mode:
```
$ ./kcores sg c ../data/graph.txt ../data/community.dat
```

## Get the output results

The ./kcores dumps the clusters in **stdout** in format:
```
node_id1 cluster_id
node_id2 cluster_id
...
```
It also dumps the timing measures in **stderr**. So in order to catch the results you can redirect the outputs into files:
```
$ # > for stdout and 2> for stderr
$ ./kcores spc s ../data/graph > clusters.dat 2> timing.txt
```

## Calculate metrics

Make sure you are inside the metrics/ folder of the project and you have compiled the metrics (see previous section). There 2 main script inside:

* evaluations.py: RI/NMI metrics
* conductance.py: conductance metrics

You can check the arguments of the scripts:

```
$ ./evaluation.py -h
usage: evaluation.py [-h] [-r REALCLUSTERSFILE] [-a ALGCLUSTERSFILE]
                     [-c CORES_FILE] [-d]

optional arguments:
  -h, --help            show this help message and exit
  -r REALCLUSTERSFILE, --real-clusters REALCLUSTERSFILE
                        File containing the real clusters.
  -a ALGCLUSTERSFILE, --alg-clusters ALGCLUSTERSFILE
                        File containing the clusters found by cluster
                        algorithms.
  -c CORES_FILE, --cores CORES_FILE
                        File containing the cores, if passed then metrics for
                        the max_core will be calculated
  -d, --do-coverage     Dump cluster coverage info.

```

And

```
$ ./conductance.py -h
usage: conductance.py [-h] -g GRAPH_FILE -c ALG_CLUSTERS_FILE

optional arguments:
  -h, --help            show this help message and exit
  -g GRAPH_FILE, --graph GRAPH_FILE
                        File containing the graph file
  -c ALG_CLUSTERS_FILE, --clusters-alg ALG_CLUSTERS_FILE
                        File containing the clusters found by an algorithm.

```

Here are some examples:

Calculate the RI/NMI metrics (in this example we will provide community  data file 2 times so 
the RI/NMI will be 1.0).

```
$ ./evaluation.py -a ../data/community.dat -r ../data/community.dat 
RI: 1.0
NMI: 1.0
$ ./evaluation.py -a ../data/community.dat -r ../data/community.dat -c ../data/cores.graph.txt
max_core-clusters: 1
max_core-RI: 0
max_core-NMI: 0
$ ./evaluation.py -a ../data/community.dat -r ../data/community.dat -c ../data/cores.graph.txt -d
...
...
cluster: 35=0.6667
cluster: 36=4.0
max_core_nodes: 14
max_core-list-clusters: 1
max_core_cluster_coverage: 9.66666666667
```

## Automate the experiments

All the experiments for all the datasets were automated through scripts but it is not plug-n-play for the moment. 
In case you want to rerun all the tests from scratch along with automatic metrics reporting you can take a look into the *tools/helpers/* and *tools/fbhelpers/* and investigate the *fbtest* and *test* scripts.


# Structure and format of the final results

This section describes the format of the final results per dataset. All the datasets along with the 
results are located in **master-bigdata.polytechnique.fr** machine under the:  
**/home/ekiagias/results/**


## Synthetic dataset results

This is the synthetic graph dataset used in the first core-cluster paper, it still has the same folder 
structure and the same graph files.

Root folder: **/home/ekiagias/results/graphdata/**  
Inside this folder there are folders with all various sizes of graphs:

``` 
100 600 1100 1600 2100 2600 3100 3600
```

Inside each graph size folder there are folders for each mixing parameter:

```
0.01  0.08  0.15  0.22  0.29  0.36  0.43
```
Inside inside every mixing parameter folder there 3 folders with the degree(%) distribution

```
10% 30% 50%
```

Finally inside every degree distribution folder there are the graph and the community files. The are 
10 graph files and 10 community files in each folder with names:

* graph_{1-10}.txt
* community_{1-10}.txt
* cores.graph_{1-10}.txt: this file contains the core number of each node.


**Results' folder and files:**

Inside the folder there is a new folder **corenew**. The latter contains all the results 
of the new experiments per algorithm. There are 2 types of files:

* clusters: files containing the clustering of the runned algorithms
* metrics: files containing the timing/RI/NMI etc results

The naming scheme of the files is the following:

```
{baseline|core}_{algorithm}_{metrics|clusters}_{1-10}.{txt|dat}
```

The algorithm value is the algorithm abbreviation same as the arguments in ./kcores (check previous sections).

So the clusters of the **standard FastGreedy (fg)** algorithm for the **graph_3.txt** input file are store in:

**baseline_fg_clusters_3.dat**

The metrics for the core mode FastGreedy (fg) algorithm for the graph_10.txt file are store in:

**core_fg_metrics_10.txt**

The naming scheme is the same for all the experiments in this dataset.

**Note**: please ingnore some files which might not follow the mentioned naming scheme and contain *spectral* in the filename, these are old experiments, they are not included in the reported results. Official spectral clustering results are stored in **spc** files (abbreviation used for spectral clustering).

### Cluster files

The cluster files have the same format as the community_{1-10}.dat

```
node_id cluster_id
```

### Metrics files

Here are examined the core metrics files (baseline metric files contain a subset of the core metrics values). 
Each core metrics file contains the following values:

* *Core decomposition time*: cpu time took for core decomposition
* *Core algorithm time*: cpu time took for the algorithm to run
* *Elapsed time*: total cpu time (core decomposition + algorithm)
* *RI*
* *NMI*
* *cluster : <cluster_id>* , each line contains the cluster_id and its node coverage % in the total graph. If you sum all the cluster: <cluster_id> values you shall get 100% (might be ~100.005 due to round ups)
* *max_core-clusters*: number of clusters in the max core
* *max_core-RI*
* *max_core-NMI*
* *max_core_nodes*: number of nodes in the max core
* *max_core-list-clusters*: cluster_ids which appear in the max core
* *max_core_cluster_coverage*: % node coverage of the max core nodes in the graph


## Facebook dataset results

This dataset was also used in the core-cluster paper. 
Root folder: **/home/ekiagias/results/fbdata**

The root folder contains a 100 folders each one representing a different facebook dataset. 
The folder name is the name of the dataset, example: the Yale4/ contains Yale.graph4.txt and it also contains all the results files for the Yale4. There are 2 files here (same as the synthetic dataset):

* clusters: files containing the clustering results of the runned algorithms
* metrics: files containing the timing and conductance results

The naming scheme of the files is the following:

```
{core|baseline}_{algorithm}_{clusters|metrics}_{dataset_name}.{dat|txt}
```

The *algorithm* valule is the algorithm abbreviations used in ./kcores (see previous sections). The clusters of standard MultiLevel algorithm for the Yale4 dataset are in the file:

**baseline_ml_clusters_Yale4.dat**

And another example, the metrics of core mode InfoMap algorithm for Yale4 are in the file:

**core_ifm_metrics_Yale4.txt**.

### Facebook dataset metrics file

Here are examined the metrics files for the core metrics files. The files contain:

* *Core decomposition time*: cpu time took for core decomposition
* *Core algorithm time*: cpu time took for the algorithm to run
* *Elapsed time*: total cpu time (core decomposition + algorithm)
* *cluster-Conductance <cluster_size>=<conductance>*: each one these lines represent a cluster. It has the size of cluster and afterwards its conductance
* *Conductance*: the average conductance of the graph

# Plotting the results

This section describes how to generate the plots out of the result files. The scripts described here located inside the tools/ directory in the project.

Because the result files are many it would be slow to parse them every to generate or change the plots so the plotting procedure breaks into two steps

1. parse the results directory and save the data
2. generate the plots from the parsed data

In the first step the result files are parsed and the data is stored into a python pickle file and afterwards the same file is loaded and the plots are generated.

There are 2 plotting scripts

* *plotz.py*: this is used to generate plots for the synthetic graph dataset
* *fbplotz.py*: this is used to generate plots for the facebook dataset

## plotz.py (synthetic data)

You can check the full list of arguments of the script:

```
$ ./plotz.py -h
```

There are many arguments so let's take it step by step. With the **-p** argument we will enable the parsing mode 
and with **-i** we will specify the results directory from where it will search and parse the results. Here we assume that we have the results from the **/home/ekiagias/results/** in our local path of the project inside the data/ folder.

```
$ ./plotz.py -p -i ../data/results/graphdata/ 
$ # in case you do not want the script to dump info in 
$ # the stdout you can also pass the -q (--quiet) argument
$ # ./plotz.py -p -i ../data/results/graphdata/ -q
```

The above will generate a **dataz.pic** file in the current local directory. Make sure you have this file:

```
$ inside the tools/ directory where you run the script
$ ls
$ # you should see a dataz.pic
```

Once we have the dataz.pic we can generate some plots. Let's make a directory where you will dump to plots:

```
$ # inside the tools/ directory
$ mkdir plots/
```

With **-t** we can generate the timing plots, with the **-n** the NMI plots and with the **-r** the RI plots. Finally with **-o** argument we specify the output directory where the plots will be stored. You 
can generate them one by one but also all together.

```
$ ./plotz.py -t -r -n -o plots/
```

Now inside the plots/ are all the timing/RI/NMI plots per algorithm and per D1/D2/D3 (degree distributions) graph files.

The latter generates the plots for all the algorithms. You can specify a single algorithm for which the plots will be generated. In order to see a list of the available algorithms (same as the algorithms abbreviation in ./kcores) you can do:

```
$ ./plotz.py -l
```

For example in order to generate the timing(-t) plot for the MCL algorithm:

```
$ ./plotz.py -t -o plots/ -a mcl
```

For example in order to generate the RI(-r) and NMI(-n) plots for the InfoMap algorithm:

```
$ ./plotz.py -n -r -o plots/ -a ifm
```

Now there are also two more different kind of plots. The  "max core NMI vs final NMI per coverage" (-C argument) 
and the "max core NMI vs final NMI per max core #clusters"(-S argument). In order to generate these kind of plots you have to parse the results directory with a different argument -s(--special-parse). You have to do:

```
$ ./plotz.py -s -i ../data/results/graphdata/
$ # will shall generate a max_core_dataz.pic in the current directory
```

The above should generate a *max_core_dataz.pic* file in the local directory. Once you have that you can generate:

* max core NMI vs final NMI per coverage plots:

```
$ ./plotz.py -C -o plots/
```

Again here you can specify a single algorithm with the -a argument, for example make the plots only for Spectral Clustering:

```
$ ./plotz.py -C -o plots/ -a spc
```

Finally you make:

* "max core NMI vs final NMI per max core #clusters":

```
$ ./plotz.py -S -o plots/
```

You can make the above plots -C/-S by also passing the **-g** argument in order to generate the D1/D2/D3 into a single plot, you can try:

```
$ ./plotz.py -S -o plots/ -g
$ # or the same for -C
$ # ./plotz.py -C -o plots/ -g
```

The rest of the *plotz.py* arguments which are not mentioned here can be ignored, they were mostly used for proof-of-concept reason and not for reporting results.

## fbplotz.py (facebook dataset)

The *fbplotz.py* script work in the same spirit as the *plotz.py* script. Again we assume that the **/home/ekiagias/results/** exists in the local project directory under the data/ folder.

You can check the available arguments with the -h argument:

```
$ ./fbplotz.py -h
```

First we have to parse the facebook dataset results:

```
$ # this shall generate a fbdataz.pic file with the parsed data
$ ./fbplotz.py -p -i ../data/results/fbdata/
```

The above should generate a **fbdataz.pic** file in the current directory. Make sure the file is there before you proceed.


You can now generate all the plots, conductance plot per cluster size (-s), the conductance plot for graph size(-c) and the time plots(-t), either make them step by step or all at one:

```
$ ./fbplotz.py -c -s -t -o plots/
```

Same as the *plotz.py* script you can generate plots for only one algorithm per time by using the **-a** argument. For example, generate the conductance per cluster size plot(-s) for Metis algorithm:

```
$ ./fbplotz.py -s -o plots/ -a metis
```

Again you check the available algorithms from the script with:

```
$ ./fbplotz.py -l
```