#!/usr/bin/env python3

from sys import stdin, stdout

def main():
	#just read from stding line by line and dump in the stdout

	for line in stdin:
		tokens = line.split()
		current_node = tokens[0]

		for node in tokens[1:]:
			stdout.write("{} {}\n".format(current_node, node))

if __name__ == "__main__":
	main()