#!/usr/bin/env python2

import os
import sys

def extract_conductance(input_file):
	with open(input_file, 'r') as fin:
		for line in fin:
			if line.startswith('Conductance'):
				return float(line.split()[1])

	return 1
	
def find_best(input_dir):
	for dataset in os.listdir(input_dir):
		min_conductance = 1
		algo_file = None
		dataset_path = os.path.join(input_dir, dataset)
		for tfile in os.listdir(dataset_path):
			mfile_path = os.path.join(dataset_path, tfile)

			if 'metrics' not in mfile_path:
				continue

			cconduct = extract_conductance(mfile_path)
			if cconduct < min_conductance:
				min_conductance = cconduct
				algo_file = tfile

		print "{}={}".format(dataset, algo_file)


if __name__ == "__main__":
	find_best(sys.argv[1])