#!/usr/bin/env python

import os
import sys
import pprint
import logging
import argparse
import pickle
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import axes3d
import matplotlib.lines as mlines
import numpy as np

logger = logging.getLogger(__name__)

algorithms = [ 'sg', 'mcl', 'metis','fg', 'ml', 'ifm', 'lp','leig', 'wt', 'spc', 'ceb' ]
factors = [0.1, 0.3, 0.5]
factors_label = {
	0.1 : 'D1',
	0.3 : 'D2',
	0.5 : 'D3'
}

legend_colors = ['g', 'b', 'r', 'c', 'y', 'm', '#999966', '#CC3300',
'#6600CC', '#003300', '#FF33CC', '#990000', '#99FF66', '#669999',
'#000000', '#993333']

results_folder = 'corenew'

algo_legends = {
	'mcl' : 'mcl',
    'metis' : 'metis',
    'ceb' : 'edge betweenness',
    'fg' : 'fast greedy',
    'ml' : 'multilevel',
    'ifm' : 'infoMap',
    'lp' : 'label propagation',
    'sg' : 'spinglass',
    'leig' : 'leading eigenvector',
    'wt' : 'walktrap',
    'spc' : 'spectral clustering'
}

class MyAxes3D(axes3d.Axes3D):

    def __init__(self, baseObject, sides_to_draw):
        self.__class__ = type(baseObject.__class__.__name__,
                              (self.__class__, baseObject.__class__),
                              {})
        self.__dict__ = baseObject.__dict__
        self.sides_to_draw = list(sides_to_draw)
        self.mouse_init()

    def set_some_features_visibility(self, visible):
        for t in self.w_zaxis.get_ticklines() + self.w_zaxis.get_ticklabels():
            t.set_visible(visible)
        self.w_zaxis.line.set_visible(visible)
        self.w_zaxis.pane.set_visible(visible)
        self.w_zaxis.label.set_visible(visible)

    def draw(self, renderer):
        # set visibility of some features False 
        self.set_some_features_visibility(False)
        # draw the axes
        super(MyAxes3D, self).draw(renderer)
        # set visibility of some features True. 
        # This could be adapted to set your features to desired visibility, 
        # e.g. storing the previous values and restoring the values
        self.set_some_features_visibility(True)

        zaxis = self.zaxis
        draw_grid_old = zaxis.axes._draw_grid
        # disable draw grid
        zaxis.axes._draw_grid = False

        tmp_planes = zaxis._PLANES

        if 'l' in self.sides_to_draw :
            # draw zaxis on the left side
            zaxis._PLANES = (tmp_planes[2], tmp_planes[3],
                             tmp_planes[0], tmp_planes[1],
                             tmp_planes[4], tmp_planes[5])
            zaxis.draw(renderer)
        if 'r' in self.sides_to_draw :
            # draw zaxis on the right side
            zaxis._PLANES = (tmp_planes[3], tmp_planes[2], 
                             tmp_planes[1], tmp_planes[0], 
                             tmp_planes[4], tmp_planes[5])
            zaxis.draw(renderer)

        zaxis._PLANES = tmp_planes

        # disable draw grid
        zaxis.axes._draw_grid = draw_grid_old

def extract_token(str_value):
	return float(str_value.split(':')[1].strip())

def extract_results_from_old_file(input_file):
	status = 0
	extracted = {}
	ri_values = []
	nmi_values = []
	with open(input_file) as fin:
		for line in fin:
			if line.startswith('RI'):
				ri_values.append(extract_token(line))
			elif line.startswith('NMI'):
				nmi_values.append(extract_token(line))
	

	if ri_values:
		extracted['RI'] = sum(ri_values)/len(ri_values)
	if nmi_values:
		extracted['NMI'] = sum(nmi_values)/len(nmi_values)
	
	return status, extracted

def extract_results_from_file(input_file, tokens):
	status = 0
	extracted = {}
	extracted['mode'] = tokens[0]
	extracted['alg'] = tokens[1]

	with open(input_file) as fin:
		for line in fin:
			if line.startswith('Core decomposition'):
				extracted['decomp_time'] = extract_token(line)
			elif line.startswith('Elapsed time'):
				extracted['time'] = extract_token(line)
			elif line.startswith('RI'):
				extracted['RI'] = extract_token(line)
			elif line.startswith('NMI'):
				extracted['NMI'] = extract_token(line)

	#this is to catch the problematic spinglass results
	if 'NMI' not in extracted or 'RI' not in extracted:
		status = -1
	elif extracted['NMI'] == 0 or extracted['RI'] == 0:
		status = -1
	
	return status, extracted

def get_initial_data_dict(old=False):
	full_dict = {}
	if old:
		algos = ['old_spc']
	else:
		algos = algorithms
	for alg in algos:
		full_dict[alg] = {}
		full_dict[alg]['baseline'] = {
					'NMI' : 0,
					'RI' : 0,
					'entries' : 0,
					'time' : 0 
					}
		full_dict[alg]['core'] = {
					'NMI' : 0,
					'RI' : 0,
					'entries' : 0,
					'time' : 0,
					'decomp_time' : 0,
					}
	return full_dict


def update_results(dataz, new_data):
	mode = new_data['mode']
	alg = new_data['alg']

	dataz[alg][mode]['RI'] += new_data['RI']
	dataz[alg][mode]['NMI'] += new_data['NMI']
	dataz[alg][mode]['time'] += new_data['time']
	dataz[alg][mode]['entries'] += 1

	if mode == 'core':
		dataz[alg][mode]['decomp_time'] += new_data['decomp_time'] 

#for-for-for-for \m/
def parse_old_dataz(input_folder,parse_core=True, initialize_data=True, dataz=None):
	if initialize_data:
		dataz = {}
	for size_fold in os.listdir(input_folder):
		if initialize_data:
			dataz[size_fold] = {}
		for mix_fold in os.listdir(os.path.join(input_folder, size_fold)):
			if initialize_data:
				dataz[size_fold][mix_fold] = {}
			for other_fold in os.listdir(os.path.join(input_folder, size_fold, mix_fold)):
				if initialize_data:
					dataz[size_fold][mix_fold][other_fold] = get_initial_data_dict(True)

				logger.info(other_fold)

				if parse_core:
					for sub_cores in os.listdir(os.path.join(input_folder, 
						size_fold, mix_fold, other_fold)):
						if not 'cores' in sub_cores:
							continue

						full_metrics_path = os.path.join(input_folder, 
						size_fold, mix_fold, other_fold, sub_cores, 'spc_stats.txt')
						
						logger.info(full_metrics_path)
						status, data_extracted = extract_results_from_old_file(full_metrics_path)

						if status < 0:
						 	continue
						alg = 'old_spc'
						mode = 'core'
						dataz[size_fold][mix_fold][other_fold][alg][mode]['RI'] += data_extracted['RI']
						dataz[size_fold][mix_fold][other_fold][alg][mode]['NMI'] += data_extracted['NMI']
						dataz[size_fold][mix_fold][other_fold][alg][mode]['entries'] += 1
				else:
					for sub_file in os.listdir(os.path.join(input_folder, 
						size_fold, mix_fold, other_fold)):
						if not 'spectral_stats' in sub_file or '_b.txt' in sub_file:
							continue

						full_metrics_path = os.path.join(input_folder, 
						size_fold, mix_fold, other_fold, sub_file)
						
						logger.info(full_metrics_path)
						status, data_extracted = extract_results_from_old_file(full_metrics_path)

						if status < 0:
						 	continue
						alg = 'old_spc'
						mode = 'baseline'
						dataz[size_fold][mix_fold][other_fold][alg][mode]['RI'] += data_extracted['RI']
						dataz[size_fold][mix_fold][other_fold][alg][mode]['NMI'] += data_extracted['NMI']
						dataz[size_fold][mix_fold][other_fold][alg][mode]['entries'] += 1

	#sys.exit(0)
	return dataz

def special_extract_from_file(input_file):
	NMI = None
	mc_NMI = None
	mc_cl_size = None
	mc_cl_coverage = None

	with open(input_file, 'r') as fin:
		for line in fin:
			if line.startswith('NMI'):
				NMI = extract_token(line)
			elif line.startswith('max_core-NMI'):
				mc_NMI = extract_token(line)
			elif line.startswith('max_core-clusters'):
				mc_cl_size = extract_token(line)
			elif line.startswith('max_core_cluster_coverage'):
				mc_cl_coverage = extract_token(line)

	return NMI, mc_NMI, mc_cl_size, mc_cl_coverage

def initialize_NMI_special_data_dict():
	dataz = {}

	for algo in algorithms:
		dataz[algo] = { 
			'NMIs' : [], 
			'max_core-NMIs' : [], 
			'max_core_clusters_sizes' : [], 
			'max_core_clusters_coverage' : [] 
		}

	return dataz

# hacky duplicate function
def max_core_NMI_parse_dataz(input_folder):
	dataz = initialize_NMI_special_data_dict()
	full_dataz = {}
	for size_fold in os.listdir(input_folder):
		full_dataz[size_fold] = {}

		for mix_fold in os.listdir(os.path.join(input_folder, size_fold)):
			full_dataz[size_fold][mix_fold] = {}

			for other_fold in os.listdir(os.path.join(input_folder, size_fold, mix_fold)):
				full_dataz[size_fold][mix_fold][other_fold] = initialize_NMI_special_data_dict()

				for res_file in os.listdir(os.path.join(input_folder, 
					size_fold, mix_fold, other_fold, results_folder)):
					
					full_file = (os.path.join(input_folder, 
						size_fold, mix_fold, other_fold, results_folder, res_file))
					
					#gets path/to/file/file_name_that --> file, name, that tokens
					#baseline(mode), algorithm, metrics(or any other)
					tokens = os.path.split(full_file)[1].split('_')
					algo = tokens[1]
					if tokens[2] == 'metrics' and tokens[1] in algorithms and tokens[0] == 'core':
						logger.info('Processing: '+full_file)
						NMI, mc_NMI, mc_cl_size, mc_cl_coverage = special_extract_from_file(full_file)

						if NMI is not None and mc_NMI is not None and mc_cl_size is not None and mc_cl_coverage is not None:
							if mc_cl_size == 0 or mc_cl_coverage == 0:
								logger.error('Errar: Suspicious file: '+full_file)
								continue

							dataz[algo]['NMIs'].append(NMI)
							dataz[algo]['max_core-NMIs'].append(mc_NMI)
							dataz[algo]['max_core_clusters_sizes'].append(mc_cl_size)
							dataz[algo]['max_core_clusters_coverage'].append(mc_cl_coverage)

							full_dataz[size_fold][mix_fold][other_fold][algo]['NMIs'].append(NMI)
							full_dataz[size_fold][mix_fold][other_fold][algo]['max_core-NMIs'].append(mc_NMI)
							full_dataz[size_fold][mix_fold][other_fold][algo]['max_core_clusters_sizes'].append(mc_cl_size)
							full_dataz[size_fold][mix_fold][other_fold][algo]['max_core_clusters_coverage'].append(mc_cl_coverage)

	return dataz, full_dataz		
	
#for-for-for-for \m/
def parse_dataz(input_folder):
	dataz = {}
	for size_fold in os.listdir(input_folder):
		dataz[size_fold] = {}
		for mix_fold in os.listdir(os.path.join(input_folder, size_fold)):

			dataz[size_fold][mix_fold] = {}
			for other_fold in os.listdir(os.path.join(input_folder, size_fold, mix_fold)):
				dataz[size_fold][mix_fold][other_fold] = get_initial_data_dict()

				for res_file in os.listdir(os.path.join(input_folder, 
					size_fold, mix_fold, other_fold, results_folder)):
					
					full_file = (os.path.join(input_folder, 
						size_fold, mix_fold, other_fold, results_folder, res_file))
					
					#gets path/to/file/file_name_that --> file, name, that tokens
					#baseline(mode), algorithm, metrics(or any other)
					tokens = os.path.split(full_file)[1].split('_')
					
					if tokens[2] == 'metrics' and tokens[1] in algorithms:
						logger.info('Processing: '+full_file)
						status, data_extracted = extract_results_from_file(full_file, tokens)

						if status < 0:
							continue

						update_results(dataz[size_fold][mix_fold][other_fold], data_extracted)
	
	return dataz

def get_total_time(dataz, alg, mode):
	total_times = []
	for gs in sorted(dataz.keys(), key=int):
		count_mix_params = 0
		per_mix_time = 0
		for mix_param in dataz[gs]:
			for other in dataz[gs][mix_param]:
				alg_mode_data = dataz[gs][mix_param][other][alg][mode]
				if alg_mode_data['entries'] == 0:
					continue
				per_mix_time += alg_mode_data['time'] / alg_mode_data['entries']
				count_mix_params += 1

		total_times.append( per_mix_time / count_mix_params )

	return total_times

def get_total_metric(dataz, alg, factor, mode, metric):
	total_metrics = {}
	for gs in dataz:
		access_key = str(int(factor*int(gs)))
		mix_params = []
		metrics = []
		for mix_param in sorted(dataz[gs].keys(), key=float):
			alg_mode_data = dataz[gs][mix_param][access_key][alg][mode]
			if alg_mode_data['entries'] == 0:
				continue
			else:
				mix_params.append(float(mix_param))
				metrics.append( alg_mode_data[metric] / alg_mode_data['entries'] )

		total_metrics[gs] = { "metrics" : metrics, "mix_params" : mix_params }

	return total_metrics

def validate_dataz_NMIs(dataz):
	for gs in dataz:
		for mix_param in dataz[gs]:
			for factor in dataz[gs][mix_param]:
				for alg in dataz[gs][mix_param][factor]:
					dat = dataz[gs][mix_param][factor][alg]
					#pprint.pprint(dat)
					print('{}-{}-{}-{}:'.format(gs, mix_param, factor, alg),
						len(dat['NMIs']), len(dat['max_core-NMIs']), 
						len(dat['max_core_clusters_sizes']), 
						len(dat['max_core_clusters_coverage']))

def get_total_metric_NMIs(dataz, alg, factor):
	total_metrics = {}
	
	for gs in dataz:
		access_key = str(int(factor*int(gs)))
		
		for mix_param in sorted(dataz[gs].keys(), key=float):
			#print(dataz[gs][mix_param].keys())
			if mix_param not in total_metrics:
				total_metrics[mix_param] = { 'NMIs' : [], 'max_core-NMIs' : [],
				'max_core_clusters_sizes' : [], 'max_core_clusters_coverage' : [] }

			if alg in dataz[gs][mix_param][access_key]:
				total_metrics[mix_param]['NMIs'] += dataz[gs][mix_param][access_key][alg]['NMIs']
				total_metrics[mix_param]['max_core-NMIs'] += dataz[gs][mix_param][access_key][alg]['max_core-NMIs']
				total_metrics[mix_param]['max_core_clusters_sizes'] += dataz[gs][mix_param][access_key][alg]['max_core_clusters_sizes']
				total_metrics[mix_param]['max_core_clusters_coverage'] += dataz[gs][mix_param][access_key][alg]['max_core_clusters_coverage']
	
	return total_metrics

def total_metric_plot_single(dataz, output_fold, metric):
	graph_sizes = sorted(dataz.keys(), key=int)

	for alg in algorithms:
		for factor in factors:
			baseline = get_total_metric(dataz, alg, factor, 'baseline', metric)
			core = get_total_metric(dataz, alg, factor, 'core', metric)

			for gs in graph_sizes:
				linec = '#50503E'
				ax = plt.gca()
				ax.yaxis.grid(linestyle='--', linewidth=1, color=linec)
				plt.axis([0,0.5,0,1])
				plt.xlabel('mixing parameter')
				plt.ylabel(metric)
				plt.title('graph size {}'.format(gs))
				plt.plot(baseline[gs]['mix_params'], baseline[gs]["metrics"], '-o', 
					label="baseline {}".format(alg), color='g', markersize=4)
				plt.plot(core[gs]['mix_params'], core[gs]["metrics"], '-s', 
					label="CoreCluster", color='b', markersize=4)
				plt.legend(loc='best', numpoints=1, fontsize=10, markerscale=0.7)
				plt.savefig(os.path.join(output_fold,"{}_{}_plot_{}_{}.png".format(alg, metric, gs, factors_label[factor])))
				#plt.show()
				plt.close()


def compare_implementation(dataz, dataz_old, output_fold):
	total_metric_compare(dataz, dataz_old, output_fold, 'RI')
	total_metric_compare(dataz, dataz_old, output_fold, 'NMI')

def total_metric_compare(dataz, dataz_old, output_fold, metric):
	graph_sizes = sorted(dataz.keys(), key=int)
	alg = 'spc'
	for mode in ['baseline', 'core']:
		for factor in factors:
			old = get_total_metric(dataz_old, 'old_spc', factor, mode, metric)
			new = get_total_metric(dataz, alg, factor, mode, metric)

			fig = plt.figure()
			ax = fig.add_subplot(1,1,1,projection='3d',axisbg='white')
			ax = fig.add_axes(MyAxes3D(ax, 'r'))
			ax.set_xlim([0, 0.5])
			ax.set_ylim([0, 4000])
			ax.set_zlim([0, 1])
			
			for gs in graph_sizes:
			    xs1 = old[gs]['mix_params']
			    ys1 = old[gs]["metrics"]

			    xs2 = new[gs]['mix_params']
			    ys2 = new[gs]["metrics"]
			    ax.plot(xs1, int(gs)*np.ones(len(xs1)), '-o', zs=ys1, zdir='z', color='g', markersize=4)
			    ax.plot(xs2, int(gs)*np.ones(len(xs2)), '-s', zs=ys2	, zdir='z', color='b', markersize=4)
			
			#ax.elev = 30
			ax.azim  = -35

			ax.w_xaxis.set_pane_color((0,0,0))
			ax.w_yaxis.set_pane_color((0,0,0))
			ax.w_zaxis.set_pane_color((0,0,0))
			plt.gca().invert_yaxis()
			plt.grid(linestyle='--')
			ax.set_xlabel('mixing parameter')
			ax.set_ylabel('graph size')
			ax.set_zlabel(metric)

			new_line = mlines.Line2D([], [], color='b', marker='s', label='new {} {} implementation'.format(mode, algo_legends[alg]))
			old_line = mlines.Line2D([], [], color='g', marker='o', label='old {} {} implementation'.format(mode, algo_legends[alg]))

			plt.yticks([0, 1000, 2000, 3000, 4000])
			#plt.grid(b=True, linestyle='dashed')
			plt.legend(handles=[old_line, new_line], numpoints=1, loc='best', fontsize=10, markerscale=0.7)
			plt.savefig(os.path.join(output_fold,"{}_{}_{}_3d_plot_compare.png".format(alg+"-"+mode, metric, factors_label[factor])))
			plt.close()
			#plt.show()

def total_metric_plot(dataz, output_fold, metric, single_plots):
	if single_plots:
		total_metric_plot_single(dataz, output_fold, metric)
		return

	graph_sizes = sorted(dataz.keys(), key=int)

	for alg in algorithms:
		for factor in factors:

			baseline = get_total_metric(dataz, alg, factor, 'baseline', metric)
			core = get_total_metric(dataz, alg, factor, 'core', metric)

			fig = plt.figure()
			ax = fig.add_subplot(1,1,1,projection='3d',axisbg='white')
			ax = fig.add_axes(MyAxes3D(ax, 'r'))
			ax.set_xlim([0, 0.5])
			ax.set_ylim([0, 4000])
			ax.set_zlim([0, 1])
			
			for gs in graph_sizes:
			    #print(len(baseline[gs]['mix_params']))
			    xs1 = baseline[gs]['mix_params']
			    ys1 = baseline[gs]["metrics"]

			    xs2 = core[gs]['mix_params']
			    ys2 = core[gs]["metrics"]
			    ax.plot(xs1, int(gs)*np.ones(len(xs1)), '-o', zs=ys1, zdir='z', color='g', markersize=4)
			    ax.plot(xs2, int(gs)*np.ones(len(xs2)), '-s', zs=ys2	, zdir='z', color='b', markersize=4)
			
			#ax.elev = 30
			ax.azim  = -35

			ax.w_xaxis.set_pane_color((0,0,0))
			ax.w_yaxis.set_pane_color((0,0,0))
			ax.w_zaxis.set_pane_color((0,0,0))
			plt.gca().invert_yaxis()
			plt.grid(linestyle='--')
			ax.set_xlabel('mixing parameter')
			ax.set_ylabel('graph size')
			ax.set_zlabel(metric)

			core_line = mlines.Line2D([], [], color='b', marker='s', label='CoreCluster')
			baseline_line = mlines.Line2D([], [], color='g', marker='o', label='baseline {}'.format(algo_legends[alg]))

			plt.yticks([0, 1000, 2000, 3000, 4000])
			#plt.grid(b=True, linestyle='dashed')
			plt.legend(handles=[baseline_line, core_line], numpoints=1, loc='best', fontsize=10, markerscale=0.7)
			plt.savefig(os.path.join(output_fold,"{}_{}_{}_3d_plot.png".format(alg, metric, factors_label[factor])))
			plt.close()
			#plt.show()
		
def total_time_plot(dataz, output_fold):
	graph_sizes = sorted(dataz.keys(), key=int)
	
	## MCL plots
	for alg in algorithms:

		baseline = get_total_time(dataz, alg, 'baseline')
		core = get_total_time(dataz, alg, 'core')

		plt.xlabel('Graph size')
		plt.ylabel('Time (sec)')
		plt.plot(graph_sizes, baseline, '-',label="baseline {}".format(algo_legends[alg]), color='g')
		plt.plot(graph_sizes, core, '--', label="CoreCluster", color='b')
		plt.legend(loc='best')
		plt.savefig(os.path.join(output_fold,"{}_time_plot.png".format(alg)))
		plt.close()


def get_total_for_factor(dataz, alg, factor):
	NMIs = []
	max_core_NMIs = []
	max_core_clusters_sizes = []
	max_core_clusters_coverage = []

	for gs in dataz:
		access_key = str(int(factor*int(gs)))
		for mix_param in dataz[gs]:
			NMIs += dataz[gs][mix_param][access_key][alg]['NMIs']
			max_core_NMIs += dataz[gs][mix_param][access_key][alg]['max_core-NMIs']
			max_core_clusters_sizes += dataz[gs][mix_param][access_key][alg]['max_core_clusters_sizes']
			max_core_clusters_coverage += dataz[gs][mix_param][access_key][alg]['max_core_clusters_coverage']

	return NMIs, max_core_NMIs, max_core_clusters_sizes, max_core_clusters_coverage


def scatter_plot_per_size_3D(dataz, output_fold):
	for alg in algorithms:
		for factor in factors:

			total_metrics = get_total_metric_NMIs(dataz, alg, factor)
			#print(total_metrics.keys())
			fig = plt.figure()
			ax = fig.add_subplot(1,1,1,projection='3d',axisbg='white')
			ax = fig.add_axes(MyAxes3D(ax, 'r'))
			# here will be the coverage
			#ax.set_xlim([0, 0.5])
			#ax.set_ylim([0, 4000])
			#ax.set_zlim([0, 1])
			
			#for gs in graph_sizes:
			for mix_param in sorted(total_metrics.keys(), key=float):
			    xs1 = total_metrics[mix_param]['max_core_clusters_sizes']
			    ys1 = total_metrics[mix_param]['NMIs']
			    
			    xs2 = total_metrics[mix_param]['max_core_clusters_sizes']
			    ys2 = total_metrics[mix_param]['max_core-NMIs']
			    
			    ax.scatter(xs1, float(mix_param)*np.ones(len(xs1)), zs=ys1, zdir='z', color='g', alpha=0.5) #markersize=4)
			    ax.scatter(xs2, float(mix_param)*np.ones(len(xs2)), zs=ys2	, zdir='z', color='b', alpha=0.5) #markersize=4)
			
			ax.azim  = -35

			ax.w_xaxis.set_pane_color((0,0,0))
			ax.w_yaxis.set_pane_color((0,0,0))
			ax.w_zaxis.set_pane_color((0,0,0))
			plt.gca().invert_yaxis()
			plt.grid(linestyle='--')
			ax.set_xlabel('#clusters in max-core')
			ax.set_ylabel('mixing parameter')
			ax.set_zlabel('NMI')

			core_line = mlines.Line2D([], [], color='b', label='max-core NMI')
			baseline_line = mlines.Line2D([], [], color='g', label='final NMI'.format(algo_legends[alg]))
			plt.title('{} {}'.format(algo_legends[alg], factors_label[factor]))
			plt.yticks([ float(x) for x in sorted(total_metrics.keys(), key=float)])
			plt.legend(handles=[baseline_line, core_line], numpoints=1, loc='best', fontsize=10, markerscale=0.7)
			plt.savefig(os.path.join(output_fold,"{}_NMIs_clusters_3d_plot_{}.png".format(alg, factors_label[factor])))
			plt.close()

def scatter_plot_per_size_factors(dataz, output_fold):
	bin_size = 10

	for alg in algorithms:
		for factor in factors:
			plt.title("{} {}".format(algo_legends[alg], factors_label[factor]))
			plt.xlabel('max-core NMI')
			plt.ylabel('final NMI')
			
			no_bin_data, bin_data = get_cluster_bins(dataz, alg, factor, bin_size)
			
			for i, no_bin in enumerate(no_bin_data):
				legend_label = str(int(no_bin))
				plt.scatter(no_bin_data[no_bin]['max_core-NMIs'], 
					no_bin_data[no_bin]['NMIs'], alpha=0.6, label=legend_label, color=legend_colors[int(no_bin)-1])

			for i, bin in enumerate(bin_data):
				legend_label = '{}- {}'.format(bin*bin_size, (bin+1)*bin_size)
				plt.scatter(bin_data[bin]['max_core-NMIs'], 
					bin_data[bin]['NMIs'], alpha=0.6, label=legend_label, color=legend_colors[bin+10-1])
				
			plt.legend(loc='best', prop={'size' : 7.8})
			plt.savefig(os.path.join(output_fold,"{}_NMIs_clusters_{}.png".format(alg, factors_label[factor])))
			plt.close()

def get_bins_simple(dataz, bin_size):
	num_of_bins = int(100/bin_size)

	bins_data = {}
	for x in range(num_of_bins):
		bins_data[x] = {'NMIs' : [], 'max_core-NMIs' : []}

	for i, cover in enumerate(dataz['max_core_clusters_coverage']):
		bin = int(cover/bin_size)

		if bin == num_of_bins:
			bin = num_of_bins - 1

		bins_data[bin]['NMIs'].append(dataz['NMIs'][i])
		bins_data[bin]['max_core-NMIs'].append(dataz['max_core-NMIs'][i])

	return bins_data

def get_bins(dataz, alg, factor, bin_size):
	num_of_bins = int(100/bin_size)

	bins_data = {}
	for x in range(num_of_bins):
		bins_data[x] = {'NMIs' : [], 'max_core-NMIs' : []}


	for gs in dataz:
		access_key = str(int(factor*int(gs)))
		for mix_param in dataz[gs]:
			coverages = dataz[gs][mix_param][access_key][alg]['max_core_clusters_coverage']
			nmis = dataz[gs][mix_param][access_key][alg]['NMIs']
			max_core_nmis = dataz[gs][mix_param][access_key][alg]['max_core-NMIs']

			for i, cover in enumerate(coverages):
				bin = int(cover/bin_size)

				if bin == num_of_bins:
					bin = num_of_bins - 1

				bins_data[bin]['NMIs'].append(nmis[i])
				bins_data[bin]['max_core-NMIs'].append(max_core_nmis[i])
	
	return bins_data

def scatter_plot_per_coverage_3D(dataz, output_fold):
	bin_size = 20
	for alg in algorithms:
		for factor in factors[2:]:

			total_metrics = get_total_metric_NMIs(dataz, alg, factor)
			#print(total_metrics.keys())
			fig = plt.figure()
			ax = fig.add_subplot(1,1,1,projection='3d',axisbg='white')
			ax = fig.add_axes(MyAxes3D(ax, 'r'))
			
			for mix_param in sorted(total_metrics.keys(), key=float):
			    bin_data = get_bins_simple(total_metrics[mix_param], bin_size)

			    for i, bin in enumerate(bin_data):
			    	xs1 = bin_data[bin]['max_core-NMIs']
			    	ys1 = bin_data[bin]['NMIs']
			    	ax.scatter(xs1, float(mix_param)*np.ones(len(xs1)), zs=ys1, zdir='z', color=legend_colors[i], alpha=0.5)
			
			ax.azim  = -35
			ax.w_xaxis.set_pane_color((0,0,0))
			ax.w_yaxis.set_pane_color((0,0,0))
			ax.w_zaxis.set_pane_color((0,0,0))
			plt.gca().invert_yaxis()
			plt.grid(linestyle='--')
			ax.set_xlabel('max-core NMI')
			ax.set_ylabel('mixing parameter')
			ax.set_zlabel('final NMI')

			legend_lists = []

			for i in range(int(100/bin_size)):
				label_l = '{}% -{}%'.format(i*bin_size, (i+1)*bin_size)
				legend_lists.append(mlines.Line2D([], [], color=legend_colors[i], label=label_l))
				
			plt.title('{} {}'.format(algo_legends[alg], factors_label[factor]))
			plt.yticks([ float(x) for x in sorted(total_metrics.keys(), key=float)])
			plt.legend(handles=legend_lists, numpoints=1, loc='best', fontsize=10, markerscale=0.7)
			plt.savefig(os.path.join(output_fold,"{}_NMIs_coverage_3d_plot_{}.png".format(alg, factors_label[factor])))
			plt.close()

def scatter_plot_per_coverage(dataz, output_fold):
	bin_size = 20

	for alg in algorithms:
		plt.title(algo_legends[alg])
		plt.xlabel('max-core NMI')
		plt.ylabel('final NMI')
		
		bin_data = get_bins_simple(dataz[alg], bin_size)

		for i, bin in enumerate(bin_data):
			legend_label = '{}-{}%'.format(bin*bin_size, (bin+1)*bin_size)
			plt.scatter(bin_data[bin]['max_core-NMIs'], 
				bin_data[bin]['NMIs'], alpha=0.6, label=legend_label, color=legend_colors[i])
			
		plt.legend(loc='best', prop={'size' : 7})
		plt.savefig(os.path.join(output_fold,"{}_NMI_cluster_coverage.png".format(alg)))
		plt.close()

def scatter_plot_per_coverage_factors(dataz, output_fold):
	bin_size = 20

	for alg in algorithms:
		for factor in factors:
			plt.title('{} {}'.format(algo_legends[alg], factors_label[factor]))
			plt.xlabel('max-core NMI')
			plt.ylabel('final NMI')
			
			bin_data = get_bins(dataz, alg, factor, bin_size)

			for i, bin in enumerate(bin_data):
				legend_label = '{}-{}%'.format(bin*bin_size, (bin+1)*bin_size)
				plt.scatter(bin_data[bin]['max_core-NMIs'], 
					bin_data[bin]['NMIs'], alpha=0.6, label=legend_label, color=legend_colors[i])

			plt.legend(loc='best', prop={'size' : 7})
			plt.savefig(os.path.join(output_fold,"{}_NMI_cluster_coverage_{}.png".format(alg, factors_label[factor])))
			plt.close()

def get_cluster_bins_simple(dataz, bin_size):
	no_bins_data = {}
	bins_data = {}

	for i, siz in enumerate(dataz['max_core_clusters_sizes']):
		if siz <= 10:
			if siz not in no_bins_data:
				no_bins_data[siz] = {'NMIs' : [], 'max_core-NMIs' : []}
			no_bins_data[siz]['NMIs'].append(dataz['NMIs'][i])
			no_bins_data[siz]['max_core-NMIs'].append(dataz['max_core-NMIs'][i])
		else:
			bin = int(siz/bin_size)

			if bin not in bins_data:
				bins_data[bin] = {'NMIs' : [], 'max_core-NMIs' : []}

			bins_data[bin]['NMIs'].append(dataz['NMIs'][i])
			bins_data[bin]['max_core-NMIs'].append(dataz['max_core-NMIs'][i])

	return no_bins_data, bins_data

def get_cluster_bins(dataz, alg, factor, bin_size):
	no_bins_data = {}
	bins_data = {}

	for gs in dataz:
		access_key = str(int(factor*int(gs)))
		for mix_param in dataz[gs]:
			clusters_num = dataz[gs][mix_param][access_key][alg]['max_core_clusters_sizes']
			nmis = dataz[gs][mix_param][access_key][alg]['NMIs']
			max_core_nmis = dataz[gs][mix_param][access_key][alg]['max_core-NMIs']

			for i, cls_num in enumerate(clusters_num):
				if cls_num <= 10:
					if cls_num not in no_bins_data:
						no_bins_data[cls_num] = {'NMIs' : [], 'max_core-NMIs' : []}

					no_bins_data[cls_num]['NMIs'].append(nmis[i])
					no_bins_data[cls_num]['max_core-NMIs'].append(max_core_nmis[i])
				else:
					bin = int(cls_num/bin_size)

					if bin not in bins_data:
						bins_data[bin] = {'NMIs' : [], 'max_core-NMIs' : []}

					bins_data[bin]['NMIs'].append(nmis[i])
					bins_data[bin]['max_core-NMIs'].append(max_core_nmis[i])
	
	return no_bins_data, bins_data

def scatter_plot_per_size(dataz, output_fold):
	
	# for alg in algorithms:
	# 	print("###", alg)
	# 	unique = set(dataz[alg]['max_core_clusters_sizes'])
	# 	print(len(unique), "#", unique)

	up_to_no_bin = 10
	bin_size = 10

	for alg in algorithms:
		plt.title(algo_legends[alg])
		plt.xlabel('max-core NMI')
		plt.ylabel('final NMI')
		
		no_bin_data, bin_data = get_cluster_bins_simple(dataz[alg], bin_size)
		
		for i, no_bin in enumerate(no_bin_data):
			legend_label = str(int(no_bin))
			plt.scatter(no_bin_data[no_bin]['max_core-NMIs'], 
				no_bin_data[no_bin]['NMIs'], alpha=0.6, label=legend_label, color=legend_colors[int(no_bin)-1])

		for i, bin in enumerate(bin_data):
			legend_label = '{}- {}'.format(bin*bin_size, (bin+1)*bin_size)
			plt.scatter(bin_data[bin]['max_core-NMIs'], 
				bin_data[bin]['NMIs'], alpha=0.6, label=legend_label, color=legend_colors[bin+10-1])
			
		plt.legend(loc='best', prop={'size' : 7.8})
		plt.savefig(os.path.join(output_fold,"{}_NMIs_clusters.png".format(alg)))
		plt.close()

def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", "--input-directory", dest="input_dir", type=str,
	                    help="Directory containing the various folder with \
	                    graph size as name")
	parser.add_argument("-t", "--time-plot", dest="plot_time", action="store_true",
	                    default=False, help="Make time plot.")
	parser.add_argument("-r", "--ri-plot", dest="plot_ri", action="store_true",
	                    default=False, help="Make ri plot.")
	parser.add_argument("-n", "--nmi-plot", dest="plot_nmi", action="store_true",
	                    default=False, help="Make nmi plot.")
	parser.add_argument("-o", "--output", dest="output_fold", type=str, default=".",
						help="Output folder to store the plots")
	parser.add_argument("-a", "--algorithm", dest="algo", type=str,
	                    help="Generate plots only for given algorithm.")
	parser.add_argument("-l", "--list-algorithms", dest="list_algos", action="store_true",
						help="List available algorithms.", default=False)
	parser.add_argument("-q", "--quiet", dest="be_quiet", action="store_true",
						help="Make the logger silent.", default=False)
	parser.add_argument("-p", "--pickle-data", dest="pickle_data", action="store_true",
						help="Pickle data(make it once then make the plots).", default=False)
	parser.add_argument("-P", "--Parse-old", dest="parse_old", action="store_true",
						help="Parse old java implementation in order to compare it \
						with the new one.", default=False)
	parser.add_argument("-c", "--compare-old", dest="compare_old", action="store_true",
						help="Compare old with new implementation.", default=False)
	parser.add_argument("-s", "--special-parse", dest="special_parse", action="store_true",
						help="Parse the max core NMI info for making the max core NMI vs final NMI.", 
						default=False)
	parser.add_argument("-C", "--Coverage-core-plots", dest="coverage_core_plots", action="store_true",
						help="Make the max core NMI vs final NMI per coverage.", 
						default=False)
	parser.add_argument("-S", "--Size-core-plots", dest="size_core_plots", action="store_true",
						help="Make the max core NMI vs final NMI per max core #clusters.", 
						default=False)
	parser.add_argument("-g", "--global-vs-NMIs", dest="do_global", action="store_true",
						help="Generate vs NMIs plots for all parameters under a single plot", 
						default=False)
	parser.add_argument("-D", "--D3-3D", dest="do_3D", action="store_true",
						help="Generate vs NMIs 3D plots", 
						default=False)
	return parser.parse_args()

def main():
	global algorithms
	args = parse_args()

	if not args.be_quiet:
		logging.basicConfig(level=logging.INFO, format='%(message)s')

	if args.pickle_data:
		logger.info("Parsing files from: {}".format(args.input_dir))
		dataz = parse_dataz(args.input_dir)
		pickle.dump(dataz, open('dataz.pic', 'wb'))
		logger.info("Done.")
		sys.exit(0)
		
	if args.parse_old:
		logger.info("Parsing old spc files from: {}".format(args.input_dir))
		core_spc_path, baseline_spc_path = args.input_dir.split(',')
		dataz = parse_old_dataz(core_spc_path)
		dataz = parse_old_dataz(baseline_spc_path, False, False, dataz)
		pickle.dump(dataz, open('dataz_old_spc.pic', 'wb'))
		logger.info("Done.")
		sys.exit(0)

	if args.compare_old:
		dataz = pickle.load(open('dataz.pic', 'rb'))
		dataz_old = pickle.load(open('dataz_old_spc.pic', 'rb'))
		compare_implementation(dataz, dataz_old, args.output_fold)
		sys.exit(0)

	if args.special_parse:
		logger.info("Max core parsing files from: {}".format(args.input_dir))
		dataz, full_dataz = max_core_NMI_parse_dataz(args.input_dir)
		pickle.dump({'dataz' : dataz, 'full_dataz' : full_dataz}, open('max_core_dataz.pic', 'wb'))
		logger.info("Done.")
		sys.exit(0)

	if args.list_algos:
		logger.info('Available algorithms:')
		pprint.pprint(algo_legends)
		sys.exit(0)

	if args.algo:
		if args.algo not in algo_legends:
			logger.error('Invalid algorithm!')
			sys.exit(-1)

		algorithms = [ args.algo ]

	if args.coverage_core_plots:
		dataz = pickle.load(open('max_core_dataz.pic', 'rb'))
		if args.do_global:
			scatter_plot_per_coverage(dataz['dataz'], args.output_fold)
		elif args.do_3D:
			scatter_plot_per_coverage_3D(dataz['full_dataz'], args.output_fold)
		else:
			scatter_plot_per_coverage_factors(dataz['full_dataz'], args.output_fold)
		sys.exit(0)

	if args.size_core_plots:
		dataz = pickle.load(open('max_core_dataz.pic', 'rb'))
		if args.do_global:
			scatter_plot_per_size(dataz['dataz'], args.output_fold)
		elif args.do_3D:
			scatter_plot_per_size_3D(dataz['full_dataz'], args.output_fold)
		else:
			scatter_plot_per_size_factors(dataz['full_dataz'], args.output_fold)
		sys.exit(0)

	dataz = pickle.load(open('dataz.pic', 'rb'))

	if args.plot_time:
		total_time_plot(dataz, args.output_fold)

	if args.plot_ri:
		total_metric_plot(dataz, args.output_fold, 'RI', False)

	if args.plot_nmi:
		total_metric_plot(dataz, args.output_fold, 'NMI', False)		

if __name__ == "__main__":
	main()
