#include "Graph.h"

#include <vector>
#include <iostream>

void DumpCores(const std::vector<int> &cores) {
    for (size_t i = 1; i < cores.size(); i++) {
        std::cout << i << " " << cores[i] << std::endl;
    }
}

int main(int argc, char * argv[]) {
    Graph* g = new Graph();

    // load the graphfile
    g->Load(argv[1]);

    // get the cores
    std::vector<int> cores = g->Coreness();

    // now print
    DumpCores(cores);
}
