#!/usr/bin/env python3

from sys import stdin, stdout

class GroupDict(dict):
    def __getitem__(self, key):
        if key not in self:
            self[key] = []
        return dict.__getitem__(self, key)

def main():
	nodez = GroupDict()

	for line in stdin:
		node_str, neighbor_str = line.split()
		start_edge = int(node_str)+1
		end_edge = int(neighbor_str)+1
		nodez[start_edge].append(end_edge)
		nodez[end_edge].append(start_edge)

	fout = stdout
	for node in sorted(nodez.keys()):
		neighbors = sorted(nodez[node])
		fout.write('%s\t%s\n' % (node, '\t'.join(map(str, neighbors))))

if __name__ == "__main__":
	main()