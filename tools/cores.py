#!/usr/bin/env python

import sys
import igraph

def get_graph(inputfile):
	edges = set()

	with open(inputfile) as fin:
		countyo  = 0

		for line in fin.readlines():
			tokens = line.split()
			node_id = int(tokens[0])-1

			for neighbor in tokens[1:]:
				edge = ( node_id, int(neighbor)-1 )
				redge = ( edge[1], edge[0] )

				if not edge in edges and not redge in edges:
					edges.add(edge)

			countyo += 1

	g = igraph.Graph(countyo)

	for e in edges:
		g.add_edge(e[0], e[1])

	return g

def graph_cores(graph):

	cores = graph.coreness()

	for i, c in enumerate(cores):
		print("{} {}".format(i+1, c))


if __name__ == "__main__":
	g = get_graph(sys.argv[1])
	graph_cores(g)
