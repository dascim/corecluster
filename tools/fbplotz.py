#!/usr/bin/env python

import os
import sys
import pprint
import logging
import argparse
import pickle
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import axes3d
import matplotlib.lines as mlines
import numpy as np

logger = logging.getLogger(__name__)

algorithms = [ 'sg', 'mcl', 'metis','fg', 'ml', 'ifm', 'lp','leig', 'wt', 'spc', 'fc' ]
#algorithms = [ 'mcl', 'fg', 'ml', 'ifm', 'lp', 'leig', 'wt' ]
factors = [0.1, 0.3, 0.5]
factors_label = {
	0.1 : 'D1',
	0.3 : 'D2',
	0.5 : 'D3'
}
results_folder = 'corenew'


algo_legends = {
	"mcl" : 'mcl',
    'metis' : 'metis',
    'ceb' : 'edge betweenness',
    'fg' : 'fast greedy',
    'ml' : 'multilevel',
    'ifm' : 'infoMap',
    'lp' : 'label propagation',
    'sg' : 'spinglass',
    'leig' : 'leading eigenvector',
    'wt' : 'walktrap',
    'spc' : 'spectral clustering'
}

def extract_token(str_value):
	return float(str_value.split(':')[1].strip())

def extract_results_from_file(input_file):
	status = 0
	extracted = {}
	per_cluster_extracted = {}

	with open(input_file) as fin:
		for line in fin:
			if line.startswith('cluster-'):
				cl_size, conduct = line.split()[1].split('=')
				cl_size = int(cl_size)

				if cl_size not in per_cluster_extracted:
					per_cluster_extracted[int(cl_size)] = { 'conductance' : 0, 'entries' : 0 }
				per_cluster_extracted[cl_size]['conductance'] += float(conduct)
				per_cluster_extracted[cl_size]['entries'] += 1
				# sub_dict = per_cluster_extracted[cl_size]
				# sub_dict['conductance'] += float(conduct)
				# sub_dict['entries'] += 1

			elif line.startswith('Core decomposition'):
				extracted['decomp_time'] = extract_token(line)
			elif line.startswith('Elapsed time'):
				extracted['time'] = extract_token(line)
			elif line.startswith('Conductance'):
				extracted['Conductance'] = extract_token(line)
			
	#this is to catch the problematic spinglass results
	if not extracted:
		status = -1

	if not 'Conductance' in extracted or extracted['Conductance'] <= 0 or not 'time' in extracted:
		status = -1

	return status, extracted, per_cluster_extracted

def get_initial_data_dict(old=False):
	full_dict = {}
	if old:
		algos = ['old_spc']
	else:
		algos = algorithms
	for alg in algos:
		full_dict[alg] = {}
		full_dict[alg]['baseline'] = {
					'Conductance' : 0,
					'entries' : 0,
					'time' : 0 
					}
		full_dict[alg]['core'] = {
					'Conductance' : 0,
					'entries' : 0,
					'time' : 0,
					'decomp_time' : 0
					}
	return full_dict

def get_initial_per_cluster():
	full_dict = {}
	for alg in algorithms:
		full_dict[alg] = { 'baseline' : {}, 'core' : {} }
	return full_dict

def wc_l(graph_file):
	lines_num = 0
	with open(graph_file, 'r') as fin:
		for line in fin:
			lines_num += 1

	return lines_num

#for-for-for-for \m/
def parse_dataz(input_folder):
	dataz = {}
	# this contains the cluster sizes along with their conductance
	per_cluster_data = get_initial_per_cluster()

	for graph_folder in os.listdir(input_folder):
		graph_folder_path = os.path.join(input_folder, graph_folder)
		graph_file_path = os.path.join(graph_folder_path, graph_folder)+'.graph.txt'
		graph_size = wc_l(graph_file_path)
		if graph_size not in dataz:
			dataz[graph_size] = get_initial_data_dict()
		
		for cluster_file in os.listdir(graph_folder_path):
			if not "metrics" in cluster_file:
				continue

			tokens = cluster_file.split('_')
			alg = tokens[1]
			if alg not in algorithms:
				continue

			full_metrics_path = os.path.join(graph_folder_path, cluster_file)
			status, data_extracted, per_cluster_extracted = extract_results_from_file(full_metrics_path)

			if status < 0:
				continue

			mode = tokens[0]
			
			for key in per_cluster_extracted:
				if key not in per_cluster_data[alg][mode]:
					per_cluster_data[alg][mode][key] = { 'conductance' : 0, 'entries' : 0 }

				per_cluster_data[alg][mode][key]['conductance'] += per_cluster_extracted[key]['conductance']
				per_cluster_data[alg][mode][key]['entries'] += per_cluster_extracted[key]['entries']


			dataz[graph_size][alg][mode]['Conductance'] += data_extracted['Conductance']
			dataz[graph_size][alg][mode]['time'] += data_extracted['time']
			dataz[graph_size][alg][mode]['entries'] += 1

	return dataz, per_cluster_data

def get_total_metric(dataz, alg, mode, metric):
	total_times = []
	gss = []
	for gs in sorted(dataz.keys(), key=int):
		dat = dataz[gs][alg][mode]
		if dat['entries'] == 0:
			continue

		gss.append(gs)
		total_times.append( dat[metric] / dat['entries'] )

	return gss, total_times

def get_total_per_cluster(per_cluster_data, alg, mode):
	data_dict = per_cluster_data[alg][mode]
	cl_sizes = []
	conductances = []
	
	bin_size = 3000
	max_cluster_size = max(data_dict.keys())
	num_of_bins = int(max_cluster_size/bin_size)

	bins_data = {}
	for x in range(num_of_bins+1):
		bins_data[x] = {'conductance' : 0, 'entries' : 0}

	for cl_size in data_dict:
		if cl_size < 10:
			continue

		bin = int(cl_size/bin_size)
		bins_data[bin]['conductance'] += data_dict[cl_size]['conductance']
		bins_data[bin]['entries'] += data_dict[cl_size]['entries']
		
	
	for bin in bins_data:
		if bins_data[bin]['entries'] == 0:
			continue

		cl_sizes.append((bin+1)*bin_size)
		conductances.append(bins_data[bin]['conductance']/bins_data[bin]['entries'])

	return cl_sizes, conductances
def per_cluster_plot(per_cluster_data, output_fold):
	## MCL plots
	for alg in algorithms:
		baseline_gss, baseline = get_total_per_cluster(per_cluster_data, alg, 'baseline')
		core_gss, core = get_total_per_cluster(per_cluster_data, alg, 'core')

		plt.xlabel('Cluster size')
		plt.ylabel('Conductance')
		plt.plot(baseline_gss, baseline, '-',label="baseline {}".format(algo_legends[alg]), color='g')
		plt.plot(core_gss, core, '--', label="CoreCluster", color='b')
		plt.legend(loc='best')
		plt.savefig(os.path.join(output_fold,"{}_conductance_per_cluster_plot.png".format(alg)))
		plt.close()

def total_conductance_plot(dataz, output_fold):
	graph_sizes = sorted(dataz.keys(), key=int)
	
	## MCL plots
	for alg in algorithms:
		baseline_gss, baseline = get_total_metric(dataz, alg, 'baseline', 'Conductance')
		core_gss, core = get_total_metric(dataz, alg, 'core', 'Conductance')

		plt.xlabel('Graph size')
		plt.ylabel('Conductance')
		plt.plot(baseline_gss, baseline, '-',label="baseline {}".format(algo_legends[alg]), color='g')
		plt.plot(core_gss, core, '--', label="CoreCluster", color='b')
		plt.legend(loc='best')
		plt.savefig(os.path.join(output_fold,"{}_conductance_plot.png".format(alg)))
		plt.close()
		
def total_time_plot(dataz, output_fold):
	graph_sizes = sorted(dataz.keys(), key=int)
	
	## MCL plots
	for alg in algorithms:
		baseline_gss, baseline = get_total_metric(dataz, alg, 'baseline', 'time')
		core_gss, core = get_total_metric(dataz, alg, 'core', 'time')

		plt.xlabel('Graph size')
		plt.ylabel('Time (sec)')
		plt.plot(baseline_gss, baseline, '-',label="baseline {}".format(algo_legends[alg]), color='g')
		plt.plot(core_gss, core, '--', label="CoreCluster", color='b')
		plt.legend(loc='best')
		plt.savefig(os.path.join(output_fold,"{}_time_plot.png".format(alg)))
		plt.close()

def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", "--input-directory", dest="input_dir", type=str,
	                    help="Directory containing the various folder with \
	                    graph size as name")
	parser.add_argument("-t", "--time-plot", dest="plot_time", action="store_true",
	                    default=False, help="Make time plot.")
	parser.add_argument("-c", "--conductance", dest="plot_conductance", action="store_true",
	                    default=False, help="Make conductance plot.")
	parser.add_argument("-s", "--sconductance", dest="per_cluster", action="store_true",
	                    default=False, help="Make conductance plot per cluster size.")
	parser.add_argument("-o", "--output", dest="output_fold", type=str, default=".",
						help="Output folder to store the plots")
	parser.add_argument("-a", "--algorithm", dest="algo", type=str,
	                    help="Generate plots only for given algorithm.")
	parser.add_argument("-l", "--list-algorithms", dest="list_algos", action="store_true",
						help="List available algorithms.", default=False)
	parser.add_argument("-q", "--quiet", dest="be_quiet", action="store_true",
						help="Make the logger silent.", default=False)
	parser.add_argument("-p", "--pickle-data", dest="pickle_data", action="store_true",
						help="Pickle data(make it once then make the plots).", default=False)
	return parser.parse_args()

def main():
	global algorithms
	args = parse_args()

	if not args.be_quiet:
		logging.basicConfig(level=logging.INFO, format='%(message)s')

	if args.pickle_data:
		logger.info("Parsing files from: {}".format(args.input_dir))
		dataz, per_cluster_data = parse_dataz(args.input_dir)
		pickle.dump({"dataz" : dataz, "per_cluster_data" : per_cluster_data }, 
			open('fbdataz.pic', 'wb'))
		#pprint.pprint(per_cluster_data)
		logger.info("Done.")
		sys.exit(0)

	dataz_dict = pickle.load(open('fbdataz.pic', 'rb'))

	#print(dataz_dict.keys())
	#pprint.pprint(dataz_dict)
	dataz = dataz_dict['dataz']
	per_cluster_data = dataz_dict['per_cluster_data']
	#sys.exit(0)
	if args.list_algos:
		logger.info('Available algorithms:')
		pprint.pprint(algo_legends)
		sys.exit(0)

	if args.algo:
		if args.algo not in algo_legends:
			logger.error('Invalid algorithm!')
			sys.exit(-1)

		algorithms = [ args.algo ]


	if args.per_cluster:
		per_cluster_plot(per_cluster_data, args.output_fold)

	if args.plot_time:
		total_time_plot(dataz, args.output_fold)

	if args.plot_conductance:
		total_conductance_plot(dataz, args.output_fold)

if __name__ == "__main__":
	main()
